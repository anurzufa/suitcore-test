// Generated by Dagger (https://google.github.io/dagger).
package com.suitcore.di.module;

import android.content.Context;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class ApplicationModule_ProvideContext$app_debugFactory implements Factory<Context> {
  private final ApplicationModule module;

  public ApplicationModule_ProvideContext$app_debugFactory(ApplicationModule module) {
    this.module = module;
  }

  @Override
  public Context get() {
    return provideInstance(module);
  }

  public static Context provideInstance(ApplicationModule module) {
    return proxyProvideContext$app_debug(module);
  }

  public static ApplicationModule_ProvideContext$app_debugFactory create(ApplicationModule module) {
    return new ApplicationModule_ProvideContext$app_debugFactory(module);
  }

  public static Context proxyProvideContext$app_debug(ApplicationModule instance) {
    return Preconditions.checkNotNull(
        instance.provideContext$app_debug(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
