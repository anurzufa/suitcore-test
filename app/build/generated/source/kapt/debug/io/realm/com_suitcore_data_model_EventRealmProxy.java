package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_suitcore_data_model_EventRealmProxy extends com.suitcore.data.model.Event
    implements RealmObjectProxy, com_suitcore_data_model_EventRealmProxyInterface {

    static final class EventColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long idIndex;
        long namaIndex;
        long tanggalIndex;
        long imageIndex;

        EventColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Event");
            this.idIndex = addColumnDetails("id", "id", objectSchemaInfo);
            this.namaIndex = addColumnDetails("nama", "nama", objectSchemaInfo);
            this.tanggalIndex = addColumnDetails("tanggal", "tanggal", objectSchemaInfo);
            this.imageIndex = addColumnDetails("image", "image", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        EventColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new EventColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final EventColumnInfo src = (EventColumnInfo) rawSrc;
            final EventColumnInfo dst = (EventColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.namaIndex = src.namaIndex;
            dst.tanggalIndex = src.tanggalIndex;
            dst.imageIndex = src.imageIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private EventColumnInfo columnInfo;
    private ProxyState<com.suitcore.data.model.Event> proxyState;

    com_suitcore_data_model_EventRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (EventColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.suitcore.data.model.Event>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.idIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(Integer value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$nama() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.namaIndex);
    }

    @Override
    public void realmSet$nama(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.namaIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.namaIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.namaIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.namaIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$tanggal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tanggalIndex);
    }

    @Override
    public void realmSet$tanggal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tanggalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tanggalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tanggalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tanggalIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$image() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.imageIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.imageIndex);
    }

    @Override
    public void realmSet$image(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.imageIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.imageIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.imageIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.imageIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Event", 4, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("nama", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("tanggal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("image", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static EventColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new EventColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Event";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Event";
    }

    @SuppressWarnings("cast")
    public static com.suitcore.data.model.Event createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.suitcore.data.model.Event obj = null;
        if (update) {
            Table table = realm.getTable(com.suitcore.data.model.Event.class);
            EventColumnInfo columnInfo = (EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("id")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_suitcore_data_model_EventRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_suitcore_data_model_EventRealmProxy) realm.createObjectInternal(com.suitcore.data.model.Event.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_suitcore_data_model_EventRealmProxy) realm.createObjectInternal(com.suitcore.data.model.Event.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_suitcore_data_model_EventRealmProxyInterface objProxy = (com_suitcore_data_model_EventRealmProxyInterface) obj;
        if (json.has("nama")) {
            if (json.isNull("nama")) {
                objProxy.realmSet$nama(null);
            } else {
                objProxy.realmSet$nama((String) json.getString("nama"));
            }
        }
        if (json.has("tanggal")) {
            if (json.isNull("tanggal")) {
                objProxy.realmSet$tanggal(null);
            } else {
                objProxy.realmSet$tanggal((String) json.getString("tanggal"));
            }
        }
        if (json.has("image")) {
            if (json.isNull("image")) {
                objProxy.realmSet$image(null);
            } else {
                objProxy.realmSet$image((int) json.getInt("image"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.suitcore.data.model.Event createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.suitcore.data.model.Event obj = new com.suitcore.data.model.Event();
        final com_suitcore_data_model_EventRealmProxyInterface objProxy = (com_suitcore_data_model_EventRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("nama")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$nama((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$nama(null);
                }
            } else if (name.equals("tanggal")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$tanggal((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$tanggal(null);
                }
            } else if (name.equals("image")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$image((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$image(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_suitcore_data_model_EventRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating uexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class), false, Collections.<String>emptyList());
        io.realm.com_suitcore_data_model_EventRealmProxy obj = new io.realm.com_suitcore_data_model_EventRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.suitcore.data.model.Event copyOrUpdate(Realm realm, EventColumnInfo columnInfo, com.suitcore.data.model.Event object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.suitcore.data.model.Event) cachedRealmObject;
        }

        com.suitcore.data.model.Event realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.suitcore.data.model.Event.class);
            long pkColumnIndex = columnInfo.idIndex;
            Number value = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, value.longValue());
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_suitcore_data_model_EventRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.suitcore.data.model.Event copy(Realm realm, EventColumnInfo columnInfo, com.suitcore.data.model.Event newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.suitcore.data.model.Event) cachedRealmObject;
        }

        com_suitcore_data_model_EventRealmProxyInterface realmObjectSource = (com_suitcore_data_model_EventRealmProxyInterface) newObject;

        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.namaIndex, realmObjectSource.realmGet$nama());
        builder.addString(columnInfo.tanggalIndex, realmObjectSource.realmGet$tanggal());
        builder.addInteger(columnInfo.imageIndex, realmObjectSource.realmGet$image());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_suitcore_data_model_EventRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.suitcore.data.model.Event object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        long tableNativePtr = table.getNativePtr();
        EventColumnInfo columnInfo = (EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$nama = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$nama();
        if (realmGet$nama != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.namaIndex, rowIndex, realmGet$nama, false);
        }
        String realmGet$tanggal = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        }
        Number realmGet$image = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$image();
        if (realmGet$image != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image.longValue(), false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        long tableNativePtr = table.getNativePtr();
        EventColumnInfo columnInfo = (EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.suitcore.data.model.Event object = null;
        while (objects.hasNext()) {
            object = (com.suitcore.data.model.Event) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$nama = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$nama();
            if (realmGet$nama != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.namaIndex, rowIndex, realmGet$nama, false);
            }
            String realmGet$tanggal = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$tanggal();
            if (realmGet$tanggal != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
            }
            Number realmGet$image = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$image();
            if (realmGet$image != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image.longValue(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.suitcore.data.model.Event object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        long tableNativePtr = table.getNativePtr();
        EventColumnInfo columnInfo = (EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        String realmGet$nama = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$nama();
        if (realmGet$nama != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.namaIndex, rowIndex, realmGet$nama, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.namaIndex, rowIndex, false);
        }
        String realmGet$tanggal = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
        }
        Number realmGet$image = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$image();
        if (realmGet$image != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.imageIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        long tableNativePtr = table.getNativePtr();
        EventColumnInfo columnInfo = (EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.suitcore.data.model.Event object = null;
        while (objects.hasNext()) {
            object = (com.suitcore.data.model.Event) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            String realmGet$nama = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$nama();
            if (realmGet$nama != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.namaIndex, rowIndex, realmGet$nama, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.namaIndex, rowIndex, false);
            }
            String realmGet$tanggal = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$tanggal();
            if (realmGet$tanggal != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
            }
            Number realmGet$image = ((com_suitcore_data_model_EventRealmProxyInterface) object).realmGet$image();
            if (realmGet$image != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.imageIndex, rowIndex, false);
            }
        }
    }

    public static com.suitcore.data.model.Event createDetachedCopy(com.suitcore.data.model.Event realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.suitcore.data.model.Event unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.suitcore.data.model.Event();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.suitcore.data.model.Event) cachedObject.object;
            }
            unmanagedObject = (com.suitcore.data.model.Event) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_suitcore_data_model_EventRealmProxyInterface unmanagedCopy = (com_suitcore_data_model_EventRealmProxyInterface) unmanagedObject;
        com_suitcore_data_model_EventRealmProxyInterface realmSource = (com_suitcore_data_model_EventRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$nama(realmSource.realmGet$nama());
        unmanagedCopy.realmSet$tanggal(realmSource.realmGet$tanggal());
        unmanagedCopy.realmSet$image(realmSource.realmGet$image());

        return unmanagedObject;
    }

    static com.suitcore.data.model.Event update(Realm realm, EventColumnInfo columnInfo, com.suitcore.data.model.Event realmObject, com.suitcore.data.model.Event newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_suitcore_data_model_EventRealmProxyInterface realmObjectTarget = (com_suitcore_data_model_EventRealmProxyInterface) realmObject;
        com_suitcore_data_model_EventRealmProxyInterface realmObjectSource = (com_suitcore_data_model_EventRealmProxyInterface) newObject;
        Table table = realm.getTable(com.suitcore.data.model.Event.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.namaIndex, realmObjectSource.realmGet$nama());
        builder.addString(columnInfo.tanggalIndex, realmObjectSource.realmGet$tanggal());
        builder.addInteger(columnInfo.imageIndex, realmObjectSource.realmGet$image());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Event = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nama:");
        stringBuilder.append(realmGet$nama() != null ? realmGet$nama() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tanggal:");
        stringBuilder.append(realmGet$tanggal() != null ? realmGet$tanggal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{image:");
        stringBuilder.append(realmGet$image() != null ? realmGet$image() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_suitcore_data_model_EventRealmProxy aEvent = (com_suitcore_data_model_EventRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aEvent.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aEvent.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aEvent.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
