package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_suitcore_data_model_GuestRealmProxy extends com.suitcore.data.model.Guest
    implements RealmObjectProxy, com_suitcore_data_model_GuestRealmProxyInterface {

    static final class GuestColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long idIndex;
        long nameIndex;
        long birthdateIndex;

        GuestColumnInfo(OsSchemaInfo schemaInfo) {
            super(3);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Guest");
            this.idIndex = addColumnDetails("id", "id", objectSchemaInfo);
            this.nameIndex = addColumnDetails("name", "name", objectSchemaInfo);
            this.birthdateIndex = addColumnDetails("birthdate", "birthdate", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        GuestColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new GuestColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final GuestColumnInfo src = (GuestColumnInfo) rawSrc;
            final GuestColumnInfo dst = (GuestColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.nameIndex = src.nameIndex;
            dst.birthdateIndex = src.birthdateIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private GuestColumnInfo columnInfo;
    private ProxyState<com.suitcore.data.model.Guest> proxyState;

    com_suitcore_data_model_GuestRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (GuestColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.suitcore.data.model.Guest>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.idIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(Integer value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$birthdate() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.birthdateIndex);
    }

    @Override
    public void realmSet$birthdate(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.birthdateIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.birthdateIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.birthdateIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.birthdateIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Guest", 3, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("birthdate", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static GuestColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new GuestColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Guest";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Guest";
    }

    @SuppressWarnings("cast")
    public static com.suitcore.data.model.Guest createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.suitcore.data.model.Guest obj = null;
        if (update) {
            Table table = realm.getTable(com.suitcore.data.model.Guest.class);
            GuestColumnInfo columnInfo = (GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("id")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_suitcore_data_model_GuestRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_suitcore_data_model_GuestRealmProxy) realm.createObjectInternal(com.suitcore.data.model.Guest.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_suitcore_data_model_GuestRealmProxy) realm.createObjectInternal(com.suitcore.data.model.Guest.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_suitcore_data_model_GuestRealmProxyInterface objProxy = (com_suitcore_data_model_GuestRealmProxyInterface) obj;
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("birthdate")) {
            if (json.isNull("birthdate")) {
                objProxy.realmSet$birthdate(null);
            } else {
                objProxy.realmSet$birthdate((String) json.getString("birthdate"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.suitcore.data.model.Guest createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.suitcore.data.model.Guest obj = new com.suitcore.data.model.Guest();
        final com_suitcore_data_model_GuestRealmProxyInterface objProxy = (com_suitcore_data_model_GuestRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else if (name.equals("birthdate")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$birthdate((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$birthdate(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_suitcore_data_model_GuestRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating uexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class), false, Collections.<String>emptyList());
        io.realm.com_suitcore_data_model_GuestRealmProxy obj = new io.realm.com_suitcore_data_model_GuestRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.suitcore.data.model.Guest copyOrUpdate(Realm realm, GuestColumnInfo columnInfo, com.suitcore.data.model.Guest object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.suitcore.data.model.Guest) cachedRealmObject;
        }

        com.suitcore.data.model.Guest realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.suitcore.data.model.Guest.class);
            long pkColumnIndex = columnInfo.idIndex;
            Number value = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, value.longValue());
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_suitcore_data_model_GuestRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.suitcore.data.model.Guest copy(Realm realm, GuestColumnInfo columnInfo, com.suitcore.data.model.Guest newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.suitcore.data.model.Guest) cachedRealmObject;
        }

        com_suitcore_data_model_GuestRealmProxyInterface realmObjectSource = (com_suitcore_data_model_GuestRealmProxyInterface) newObject;

        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.nameIndex, realmObjectSource.realmGet$name());
        builder.addString(columnInfo.birthdateIndex, realmObjectSource.realmGet$birthdate());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_suitcore_data_model_GuestRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.suitcore.data.model.Guest object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        long tableNativePtr = table.getNativePtr();
        GuestColumnInfo columnInfo = (GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        String realmGet$birthdate = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$birthdate();
        if (realmGet$birthdate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.birthdateIndex, rowIndex, realmGet$birthdate, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        long tableNativePtr = table.getNativePtr();
        GuestColumnInfo columnInfo = (GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.suitcore.data.model.Guest object = null;
        while (objects.hasNext()) {
            object = (com.suitcore.data.model.Guest) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
            String realmGet$birthdate = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$birthdate();
            if (realmGet$birthdate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.birthdateIndex, rowIndex, realmGet$birthdate, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.suitcore.data.model.Guest object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        long tableNativePtr = table.getNativePtr();
        GuestColumnInfo columnInfo = (GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        String realmGet$birthdate = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$birthdate();
        if (realmGet$birthdate != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.birthdateIndex, rowIndex, realmGet$birthdate, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.birthdateIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        long tableNativePtr = table.getNativePtr();
        GuestColumnInfo columnInfo = (GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.suitcore.data.model.Guest object = null;
        while (objects.hasNext()) {
            object = (com.suitcore.data.model.Guest) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
            String realmGet$birthdate = ((com_suitcore_data_model_GuestRealmProxyInterface) object).realmGet$birthdate();
            if (realmGet$birthdate != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.birthdateIndex, rowIndex, realmGet$birthdate, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.birthdateIndex, rowIndex, false);
            }
        }
    }

    public static com.suitcore.data.model.Guest createDetachedCopy(com.suitcore.data.model.Guest realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.suitcore.data.model.Guest unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.suitcore.data.model.Guest();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.suitcore.data.model.Guest) cachedObject.object;
            }
            unmanagedObject = (com.suitcore.data.model.Guest) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_suitcore_data_model_GuestRealmProxyInterface unmanagedCopy = (com_suitcore_data_model_GuestRealmProxyInterface) unmanagedObject;
        com_suitcore_data_model_GuestRealmProxyInterface realmSource = (com_suitcore_data_model_GuestRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$birthdate(realmSource.realmGet$birthdate());

        return unmanagedObject;
    }

    static com.suitcore.data.model.Guest update(Realm realm, GuestColumnInfo columnInfo, com.suitcore.data.model.Guest realmObject, com.suitcore.data.model.Guest newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_suitcore_data_model_GuestRealmProxyInterface realmObjectTarget = (com_suitcore_data_model_GuestRealmProxyInterface) realmObject;
        com_suitcore_data_model_GuestRealmProxyInterface realmObjectSource = (com_suitcore_data_model_GuestRealmProxyInterface) newObject;
        Table table = realm.getTable(com.suitcore.data.model.Guest.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.nameIndex, realmObjectSource.realmGet$name());
        builder.addString(columnInfo.birthdateIndex, realmObjectSource.realmGet$birthdate());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Guest = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{birthdate:");
        stringBuilder.append(realmGet$birthdate() != null ? realmGet$birthdate() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_suitcore_data_model_GuestRealmProxy aGuest = (com_suitcore_data_model_GuestRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aGuest.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aGuest.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aGuest.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
