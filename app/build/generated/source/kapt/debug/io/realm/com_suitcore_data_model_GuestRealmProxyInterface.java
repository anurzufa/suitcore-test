package io.realm;


public interface com_suitcore_data_model_GuestRealmProxyInterface {
    public Integer realmGet$id();
    public void realmSet$id(Integer value);
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$birthdate();
    public void realmSet$birthdate(String value);
}
