package io.realm;


import android.util.JsonReader;
import io.realm.ImportFlag;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>(5);
        modelClasses.add(com.suitcore.data.model.Event.class);
        modelClasses.add(com.suitcore.data.model.Guest.class);
        modelClasses.add(com.suitcore.data.model.Place.class);
        modelClasses.add(com.suitcore.data.model.Test.class);
        modelClasses.add(com.suitcore.data.model.User.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>(5);
        infoMap.put(com.suitcore.data.model.Event.class, io.realm.com_suitcore_data_model_EventRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.suitcore.data.model.Guest.class, io.realm.com_suitcore_data_model_GuestRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.suitcore.data.model.Place.class, io.realm.com_suitcore_data_model_PlaceRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.suitcore.data.model.Test.class, io.realm.com_suitcore_data_model_TestRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.suitcore.data.model.User.class, io.realm.com_suitcore_data_model_UserRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo createColumnInfo(Class<? extends RealmModel> clazz, OsSchemaInfo schemaInfo) {
        checkClass(clazz);

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            return io.realm.com_suitcore_data_model_EventRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            return io.realm.com_suitcore_data_model_GuestRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            return io.realm.com_suitcore_data_model_PlaceRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            return io.realm.com_suitcore_data_model_TestRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            return io.realm.com_suitcore_data_model_UserRealmProxy.createColumnInfo(schemaInfo);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getSimpleClassNameImpl(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            return "Event";
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            return "Guest";
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            return "Place";
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            return "Test";
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            return "User";
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(com.suitcore.data.model.Event.class)) {
                return clazz.cast(new io.realm.com_suitcore_data_model_EventRealmProxy());
            }
            if (clazz.equals(com.suitcore.data.model.Guest.class)) {
                return clazz.cast(new io.realm.com_suitcore_data_model_GuestRealmProxy());
            }
            if (clazz.equals(com.suitcore.data.model.Place.class)) {
                return clazz.cast(new io.realm.com_suitcore_data_model_PlaceRealmProxy());
            }
            if (clazz.equals(com.suitcore.data.model.Test.class)) {
                return clazz.cast(new io.realm.com_suitcore_data_model_TestRealmProxy());
            }
            if (clazz.equals(com.suitcore.data.model.User.class)) {
                return clazz.cast(new io.realm.com_suitcore_data_model_UserRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            com_suitcore_data_model_EventRealmProxy.EventColumnInfo columnInfo = (com_suitcore_data_model_EventRealmProxy.EventColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Event.class);
            return clazz.cast(io.realm.com_suitcore_data_model_EventRealmProxy.copyOrUpdate(realm, columnInfo, (com.suitcore.data.model.Event) obj, update, cache, flags));
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            com_suitcore_data_model_GuestRealmProxy.GuestColumnInfo columnInfo = (com_suitcore_data_model_GuestRealmProxy.GuestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Guest.class);
            return clazz.cast(io.realm.com_suitcore_data_model_GuestRealmProxy.copyOrUpdate(realm, columnInfo, (com.suitcore.data.model.Guest) obj, update, cache, flags));
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            com_suitcore_data_model_PlaceRealmProxy.PlaceColumnInfo columnInfo = (com_suitcore_data_model_PlaceRealmProxy.PlaceColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Place.class);
            return clazz.cast(io.realm.com_suitcore_data_model_PlaceRealmProxy.copyOrUpdate(realm, columnInfo, (com.suitcore.data.model.Place) obj, update, cache, flags));
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            com_suitcore_data_model_TestRealmProxy.TestColumnInfo columnInfo = (com_suitcore_data_model_TestRealmProxy.TestColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.Test.class);
            return clazz.cast(io.realm.com_suitcore_data_model_TestRealmProxy.copyOrUpdate(realm, columnInfo, (com.suitcore.data.model.Test) obj, update, cache, flags));
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            com_suitcore_data_model_UserRealmProxy.UserColumnInfo columnInfo = (com_suitcore_data_model_UserRealmProxy.UserColumnInfo) realm.getSchema().getColumnInfo(com.suitcore.data.model.User.class);
            return clazz.cast(io.realm.com_suitcore_data_model_UserRealmProxy.copyOrUpdate(realm, columnInfo, (com.suitcore.data.model.User) obj, update, cache, flags));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            io.realm.com_suitcore_data_model_EventRealmProxy.insert(realm, (com.suitcore.data.model.Event) object, cache);
        } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            io.realm.com_suitcore_data_model_GuestRealmProxy.insert(realm, (com.suitcore.data.model.Guest) object, cache);
        } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
            io.realm.com_suitcore_data_model_PlaceRealmProxy.insert(realm, (com.suitcore.data.model.Place) object, cache);
        } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
            io.realm.com_suitcore_data_model_TestRealmProxy.insert(realm, (com.suitcore.data.model.Test) object, cache);
        } else if (clazz.equals(com.suitcore.data.model.User.class)) {
            io.realm.com_suitcore_data_model_UserRealmProxy.insert(realm, (com.suitcore.data.model.User) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.suitcore.data.model.Event.class)) {
                io.realm.com_suitcore_data_model_EventRealmProxy.insert(realm, (com.suitcore.data.model.Event) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
                io.realm.com_suitcore_data_model_GuestRealmProxy.insert(realm, (com.suitcore.data.model.Guest) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
                io.realm.com_suitcore_data_model_PlaceRealmProxy.insert(realm, (com.suitcore.data.model.Place) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
                io.realm.com_suitcore_data_model_TestRealmProxy.insert(realm, (com.suitcore.data.model.Test) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.User.class)) {
                io.realm.com_suitcore_data_model_UserRealmProxy.insert(realm, (com.suitcore.data.model.User) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.suitcore.data.model.Event.class)) {
                    io.realm.com_suitcore_data_model_EventRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
                    io.realm.com_suitcore_data_model_GuestRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
                    io.realm.com_suitcore_data_model_PlaceRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
                    io.realm.com_suitcore_data_model_TestRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.User.class)) {
                    io.realm.com_suitcore_data_model_UserRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            io.realm.com_suitcore_data_model_EventRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Event) obj, cache);
        } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            io.realm.com_suitcore_data_model_GuestRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Guest) obj, cache);
        } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
            io.realm.com_suitcore_data_model_PlaceRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Place) obj, cache);
        } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
            io.realm.com_suitcore_data_model_TestRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Test) obj, cache);
        } else if (clazz.equals(com.suitcore.data.model.User.class)) {
            io.realm.com_suitcore_data_model_UserRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.User) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.suitcore.data.model.Event.class)) {
                io.realm.com_suitcore_data_model_EventRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Event) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
                io.realm.com_suitcore_data_model_GuestRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Guest) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
                io.realm.com_suitcore_data_model_PlaceRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Place) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
                io.realm.com_suitcore_data_model_TestRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.Test) object, cache);
            } else if (clazz.equals(com.suitcore.data.model.User.class)) {
                io.realm.com_suitcore_data_model_UserRealmProxy.insertOrUpdate(realm, (com.suitcore.data.model.User) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.suitcore.data.model.Event.class)) {
                    io.realm.com_suitcore_data_model_EventRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Guest.class)) {
                    io.realm.com_suitcore_data_model_GuestRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Place.class)) {
                    io.realm.com_suitcore_data_model_PlaceRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.Test.class)) {
                    io.realm.com_suitcore_data_model_TestRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.suitcore.data.model.User.class)) {
                    io.realm.com_suitcore_data_model_UserRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_EventRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_GuestRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_PlaceRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_TestRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_UserRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_EventRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_GuestRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_PlaceRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_TestRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_UserRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(com.suitcore.data.model.Event.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_EventRealmProxy.createDetachedCopy((com.suitcore.data.model.Event) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.suitcore.data.model.Guest.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_GuestRealmProxy.createDetachedCopy((com.suitcore.data.model.Guest) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.suitcore.data.model.Place.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_PlaceRealmProxy.createDetachedCopy((com.suitcore.data.model.Place) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.suitcore.data.model.Test.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_TestRealmProxy.createDetachedCopy((com.suitcore.data.model.Test) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.suitcore.data.model.User.class)) {
            return clazz.cast(io.realm.com_suitcore_data_model_UserRealmProxy.createDetachedCopy((com.suitcore.data.model.User) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}
