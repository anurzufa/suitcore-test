package io.realm;


public interface com_suitcore_data_model_EventRealmProxyInterface {
    public Integer realmGet$id();
    public void realmSet$id(Integer value);
    public String realmGet$nama();
    public void realmSet$nama(String value);
    public String realmGet$tanggal();
    public void realmSet$tanggal(String value);
    public Integer realmGet$image();
    public void realmSet$image(Integer value);
}
