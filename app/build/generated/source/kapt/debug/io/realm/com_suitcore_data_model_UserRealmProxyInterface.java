package io.realm;


public interface com_suitcore_data_model_UserRealmProxyInterface {
    public Integer realmGet$id();
    public void realmSet$id(Integer value);
    public String realmGet$nama();
    public void realmSet$nama(String value);
}
