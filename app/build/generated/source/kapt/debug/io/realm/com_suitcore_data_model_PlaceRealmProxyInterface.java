package io.realm;


public interface com_suitcore_data_model_PlaceRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public Integer realmGet$latitude();
    public void realmSet$latitude(Integer value);
    public Integer realmGet$longitude();
    public void realmSet$longitude(Integer value);
    public Integer realmGet$image();
    public void realmSet$image(Integer value);
}
