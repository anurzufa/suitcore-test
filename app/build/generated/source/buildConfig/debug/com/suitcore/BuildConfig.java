/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.suitcore;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.suitcore";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "0.1.0-alpha.1";
  // Fields from build type: debug
  public static final String BASE_URL = "http://www.mocky.io/v2/";
  public static final String ENCRYPT_KEY = "su!tmobilE777";
}
