# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:64
-keep class androidx.core.app.CoreComponentFactory { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:356
-keep class androidx.lifecycle.ProcessLifecycleOwnerInitializer { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:347
-keep class com.facebook.CampaignTrackingReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:340
-keep class com.facebook.CurrentAccessTokenExpirationBroadcastReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:94
-keep class com.facebook.CustomTabActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:131
-keep class com.facebook.CustomTabMainActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:89
-keep class com.facebook.FacebookActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:335
-keep class com.facebook.internal.FacebookInitProvider { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:244
-keep class com.google.android.gms.auth.api.signin.RevocationBoundService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:235
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:293
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:303
-keep class com.google.android.gms.measurement.AppMeasurementInstallReferrerReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:317
-keep class com.google.android.gms.measurement.AppMeasurementJobService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:298
-keep class com.google.android.gms.measurement.AppMeasurementReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:313
-keep class com.google.android.gms.measurement.AppMeasurementService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:249
-keep class com.google.firebase.auth.internal.FederatedSignInActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:257
-keep class com.google.firebase.components.ComponentDiscoveryService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:278
-keep class com.google.firebase.iid.FirebaseInstanceIdReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:227
-keep class com.google.firebase.messaging.FirebaseMessagingService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:287
-keep class com.google.firebase.provider.FirebaseInitProvider { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:362
-keep class com.jakewharton.processphoenix.ProcessPhoenix { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:214
-keep class com.mapbox.android.telemetry.provider.MapboxTelemetryInitProvider { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:130
-keep class com.mapbox.services.android.navigation.ui.v5.MapboxNavigationActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:133
-keep class com.mapbox.services.android.navigation.v5.navigation.NavigationService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:134
-keep class com.mapbox.services.android.telemetry.service.TelemetryService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:179
-keep class com.onesignal.BootUpReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:143
-keep class com.onesignal.GcmBroadcastReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:157
-keep class com.onesignal.GcmIntentJobService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:156
-keep class com.onesignal.GcmIntentService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:154
-keep class com.onesignal.NotificationOpenedReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:177
-keep class com.onesignal.NotificationRestoreService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:173
-keep class com.onesignal.PermissionsActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:160
-keep class com.onesignal.RestoreJobService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:163
-keep class com.onesignal.RestoreKickoffJobService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:169
-keep class com.onesignal.SyncJobService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:166
-keep class com.onesignal.SyncService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:185
-keep class com.onesignal.UpgradeReceiver { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:219
-keep class com.squareup.picasso.PicassoProvider { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:64
-keep class com.suitcore.BaseApplication { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:81
-keep class com.suitcore.feature.event.EventActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:80
-keep class com.suitcore.feature.guest.GuestActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:79
-keep class com.suitcore.feature.login.LoginActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:78
-keep class com.suitcore.feature.main.MainActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:77
-keep class com.suitcore.feature.maps.MapsActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:82
-keep class com.suitcore.feature.splashscreen.SplashScreenActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:107
-keep class com.suitcore.firebase.fcm.FCMFireBaseMessagingService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:117
-keep class com.suitcore.onesignal.NotificationExtenderBareBones { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:208
-keep class com.twitter.sdk.android.core.identity.OAuthActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:198
-keep class com.twitter.sdk.android.tweetcomposer.ComposerActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:203
-keep class com.twitter.sdk.android.tweetcomposer.TweetUploadService { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:195
-keep class com.twitter.sdk.android.tweetui.GalleryActivity { <init>(); }
# Referenced at D:\Suitmedia\suitcore-example\app\build\intermediates\merged_manifests\debug\AndroidManifest.xml:191
-keep class com.twitter.sdk.android.tweetui.PlayerActivity { <init>(); }
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_title_material.xml:56
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_material.xml:52
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_button_bar_material.xml:43
-keep class android.widget.Space { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_select_dialog_material.xml:23
-keep class androidx.appcompat.app.AlertController$RecycleListView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_action_menu_item_layout.xml:17
-keep class androidx.appcompat.view.menu.ActionMenuItemView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_expanded_menu_layout.xml:17
-keep class androidx.appcompat.view.menu.ExpandedMenuView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_popup_menu_item_layout.xml:17
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_list_menu_item_layout.xml:17
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_cascading_menu_item_layout.xml:20
-keep class androidx.appcompat.view.menu.ListMenuItemView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_toolbar.xml:27
-keep class androidx.appcompat.widget.ActionBarContainer { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_toolbar.xml:43
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_action_mode_bar.xml:19
-keep class androidx.appcompat.widget.ActionBarContextView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_toolbar.xml:17
-keep class androidx.appcompat.widget.ActionBarOverlayLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_action_menu_layout.xml:17
-keep class androidx.appcompat.widget.ActionMenuView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_activity_chooser_view.xml:19
-keep class androidx.appcompat.widget.ActivityChooserView$InnerLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_material.xml:18
-keep class androidx.appcompat.widget.AlertDialogLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\288afcfb83b80e753a1de5b471865ec5\jetified-mapbox-android-sdk-7.3.0\res\layout\mapbox_mapview_internal.xml:10
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\46ca992b85e2c922fc2a45f7dc503b1e\jetified-facebook-common-5.2.0\res\layout\com_facebook_smart_device_dialog_fragment.xml:47
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\46ca992b85e2c922fc2a45f7dc503b1e\jetified-facebook-common-5.2.0\res\layout\com_facebook_device_auth_dialog_fragment.xml:45
-keep class androidx.appcompat.widget.AppCompatImageView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_button_bar_material.xml:26
-keep class androidx.appcompat.widget.ButtonBarLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_content_include.xml:19
-keep class androidx.appcompat.widget.ContentFrameLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_title_material.xml:45
-keep class androidx.appcompat.widget.DialogTitle { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_simple_overlay_action_mode.xml:23
-keep class androidx.appcompat.widget.FitWindowsFrameLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_simple.xml:17
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_dialog_title_material.xml:22
-keep class androidx.appcompat.widget.FitWindowsLinearLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_search_view.xml:75
-keep class androidx.appcompat.widget.SearchView$SearchAutoComplete { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout-v26\abc_screen_toolbar.xml:37
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_test.xml:15
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_maps.xml:9
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_guest.xml:9
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_event.xml:10
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_toolbar.xml:36
-keep class androidx.appcompat.widget.Toolbar { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_simple_overlay_action_mode.xml:32
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_screen_simple.xml:25
-keep class androidx.appcompat.widget.ViewStubCompat { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\52aae7c1c9297daa0c4d9d16498d1c1e\browser-1.0.0\res\layout\browser_actions_context_menu_page.xml:5
-keep class androidx.browser.browseractions.BrowserActionsFallbackMenuView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\recenter_btn_layout.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_guest.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_event_shimmer.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_event.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\46ca992b85e2c922fc2a45f7dc503b1e\jetified-facebook-common-5.2.0\res\layout\com_facebook_smart_device_dialog_fragment.xml:22
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\46ca992b85e2c922fc2a45f7dc503b1e\jetified-facebook-common-5.2.0\res\layout\com_facebook_device_auth_dialog_fragment.xml:22
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\alert_view_layout.xml:2
-keep class androidx.cardview.widget.CardView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\summary_peek_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\summary_content_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_viewholder_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout_alt.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_content_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-v21\feedback_viewholder_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\summary_peek_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\summary_content_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\sub_instruction_layout.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_shimmer_guest.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_shimmer_event.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_base_shimmer.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_base_error.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_base_empty.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_guest.xml:15
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_event_shimmer.xml:11
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\item_event.xml:13
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_viewholder_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_content_layout.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\fragment_test.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\feedback_bottom_sheet_layout.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\dialog_loading.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_test.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_splashscreen.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\activity_navigation.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_member.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_maps.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_main.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_guest.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_first.xml:2
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_event.xml:2
-keep class androidx.constraintlayout.widget.ConstraintLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_content_layout.xml:9
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\sub_instruction_layout.xml:41
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_viewholder_layout.xml:20
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_content_layout.xml:10
-keep class androidx.constraintlayout.widget.Guideline { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:2
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_bottom_sheet_dialog.xml:26
-keep class androidx.coordinatorlayout.widget.CoordinatorLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\d8aad3942bacafc5f13068de9c9669c5\appcompat-1.0.2\res\layout\abc_alert_dialog_material.xml:41
-keep class androidx.core.widget.NestedScrollView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\feedback_bottom_sheet_layout.xml:10
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\turn_lane_layout.xml:11
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_list_layout.xml:11
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\feedback_bottom_sheet_layout.xml:9
-keep class androidx.recyclerview.widget.RecyclerView { <init>(...); }

# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_maps.xml:20
-keep class androidx.viewpager.widget.ViewPager { <init>(...); }

# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_shimmer_guest.xml:7
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_shimmer_event.xml:7
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\layout_base_shimmer.xml:8
-keep class com.facebook.shimmer.ShimmerFrameLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\sound_layout.xml:27
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\feedback_button_layout.xml:8
-keep class com.google.android.material.floatingactionbutton.FloatingActionButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_bottom_navigation_item.xml:27
-keep class com.google.android.material.internal.BaselineLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_text_input_password_icon.xml:18
-keep class com.google.android.material.internal.CheckableImageButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_navigation_item.xml:17
-keep class com.google.android.material.internal.NavigationMenuItemView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_navigation_menu.xml:17
-keep class com.google.android.material.internal.NavigationMenuView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\mtrl_layout_snackbar.xml:18
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_layout_snackbar.xml:18
-keep class com.google.android.material.snackbar.Snackbar$SnackbarLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\mtrl_layout_snackbar_include.xml:18
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\8f4274580f77653a69f4a811c93e5c57\material-1.0.0\res\layout\design_layout_snackbar_include.xml:18
-keep class com.google.android.material.snackbar.SnackbarContentLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\85d151635550c259959f5d22946e687a\jetified-xrecyclerview-1.5.9\res\layout\listview_header.xml:60
-keep class com.jcodecraeer.xrecyclerview.SimpleViewSwitcher { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\288afcfb83b80e753a1de5b471865ec5\jetified-mapbox-android-sdk-7.3.0\res\layout\mapbox_infowindow_content.xml:2
-keep class com.mapbox.mapboxsdk.annotations.BubbleLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:10
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_maps.xml:27
-keep class com.mapbox.mapboxsdk.maps.MapView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\288afcfb83b80e753a1de5b471865ec5\jetified-mapbox-android-sdk-7.3.0\res\layout\mapbox_mapview_internal.xml:4
-keep class com.mapbox.mapboxsdk.maps.widgets.CompassView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout.xml:161
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_view_layout.xml:70
-keep class com.mapbox.services.android.navigation.ui.v5.FeedbackButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\activity_navigation.xml:8
-keep class com.mapbox.services.android.navigation.ui.v5.NavigationView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:30
-keep class com.mapbox.services.android.navigation.ui.v5.RecenterButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout.xml:154
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_view_layout.xml:60
-keep class com.mapbox.services.android.navigation.ui.v5.SoundButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:49
-keep class com.mapbox.services.android.navigation.ui.v5.instruction.InstructionView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout.xml:127
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_view_layout.xml:48
-keep class com.mapbox.services.android.navigation.ui.v5.instruction.NavigationAlertView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout_alt.xml:28
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout-land\instruction_layout.xml:28
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\sub_instruction_layout.xml:11
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\instruction_content_layout.xml:25
-keep class com.mapbox.services.android.navigation.ui.v5.instruction.maneuver.ManeuverView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\turn_lane_listitem_layout.xml:9
-keep class com.mapbox.services.android.navigation.ui.v5.instruction.turnlane.TurnLaneView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:40
-keep class com.mapbox.services.android.navigation.ui.v5.map.WayNameView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\b67f4c4548c407a13ae54dd9aaf87f30\jetified-mapbox-android-navigation-ui-0.30.0\res\layout\navigation_view_layout.xml:24
-keep class com.mapbox.services.android.navigation.ui.v5.summary.SummaryBottomSheet { <init>(...); }

# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_member.xml:33
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_guest.xml:21
# Referenced at D:\Suitmedia\suitcore-example\app\src\main\res\layout\activity_event.xml:22
-keep class com.suitcore.base.ui.recyclerview.BaseRecyclerView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\61c4f804b8601f4beb41088927f21585\tweet-composer-3.3.0\res\layout\tw__activity_composer.xml:21
-keep class com.twitter.sdk.android.tweetcomposer.ComposerView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\61c4f804b8601f4beb41088927f21585\tweet-composer-3.3.0\res\layout\tw__composer_view.xml:58
-keep class com.twitter.sdk.android.tweetcomposer.internal.util.ObservableScrollView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__action_bar.xml:28
-keep class com.twitter.sdk.android.tweetui.ToggleImageButton { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__action_bar.xml:18
-keep class com.twitter.sdk.android.tweetui.TweetActionBarView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_quote.xml:51
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_compact.xml:72
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet.xml:23
-keep class com.twitter.sdk.android.tweetui.internal.AspectRatioFrameLayout { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_quote.xml:62
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_compact.xml:83
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet.xml:31
-keep class com.twitter.sdk.android.tweetui.internal.MediaBadgeView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_quote.xml:58
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet_compact.xml:79
# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__tweet.xml:27
-keep class com.twitter.sdk.android.tweetui.internal.TweetMediaView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__player_activity.xml:32
-keep class com.twitter.sdk.android.tweetui.internal.VideoControlView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__player_activity.xml:20
-keep class com.twitter.sdk.android.tweetui.internal.VideoView { <init>(...); }

# Referenced at C:\Users\Alfiansyah Nur Zufa\.gradle\caches\transforms-2\files-2.1\acd12434af9624fea8d524c9dfbe8845\jetified-tweet-ui-3.3.0\res\layout\tw__gallery_activity.xml:22
-keep class com.twitter.sdk.android.tweetui.internal.ViewPagerFixed { <init>(...); }

