package com.suitcore.base.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u000e\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u0002H\u00020\u0004:\u000201B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0013\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0013J\u001b\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0014\u0010\u0010\u001a\u00020\u00112\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007J\u0014\u0010\u0018\u001a\u00020\u00112\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007J\u0013\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0013J\u0014\u0010\u0019\u001a\u00020\u00112\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007J\u0006\u0010\u001a\u001a\u00020\u0011J\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u000bJ\u0013\u0010\b\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u0015H\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\b\u0010\u001f\u001a\u00020\u0015H$J\u0010\u0010 \u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0004J\u001d\u0010%\u001a\u00020\u00112\u0006\u0010&\u001a\u00028\u00012\u0006\u0010\u0014\u001a\u00020\u0015H\u0016\u00a2\u0006\u0002\u0010\'J\u001d\u0010(\u001a\u00028\u00012\u0006\u0010#\u001a\u00020$2\u0006\u0010)\u001a\u00020\u0015H&\u00a2\u0006\u0002\u0010*J\u0013\u0010+\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0013J\u000e\u0010+\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010,\u001a\u00020\u00112\u0006\u0010-\u001a\u00020\rJ\u000e\u0010.\u001a\u00020\u00112\u0006\u0010/\u001a\u00020\u000fR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u00078F\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00062"}, d2 = {"Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter;", "Data", "Holder", "Lcom/suitcore/base/ui/adapter/viewholder/BaseItemViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "()V", "data", "", "getData", "()Ljava/util/List;", "mData", "Ljava/util/ArrayList;", "mItemClickListener", "Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter$OnItemClickListener;", "mLongItemClickListener", "Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter$OnLongItemClickListener;", "add", "", "item", "(Ljava/lang/Object;)V", "position", "", "(Ljava/lang/Object;I)V", "items", "addAll", "addOrUpdate", "clear", "(I)Ljava/lang/Object;", "getItemCount", "getItemId", "", "getItemResourceLayout", "getItemViewType", "getView", "Landroid/view/View;", "parent", "Landroid/view/ViewGroup;", "onBindViewHolder", "holder", "(Lcom/suitcore/base/ui/adapter/viewholder/BaseItemViewHolder;I)V", "onCreateViewHolder", "viewType", "(Landroid/view/ViewGroup;I)Lcom/suitcore/base/ui/adapter/viewholder/BaseItemViewHolder;", "remove", "setOnItemClickListener", "itemClickListener", "setOnLongItemClickListener", "longItemClickListener", "OnItemClickListener", "OnLongItemClickListener", "app_debug"})
public abstract class BaseRecyclerAdapter<Data extends java.lang.Object, Holder extends com.suitcore.base.ui.adapter.viewholder.BaseItemViewHolder<? super Data>> extends androidx.recyclerview.widget.RecyclerView.Adapter<Holder> {
    private java.util.ArrayList<Data> mData;
    private com.suitcore.base.ui.adapter.BaseRecyclerAdapter.OnItemClickListener mItemClickListener;
    private com.suitcore.base.ui.adapter.BaseRecyclerAdapter.OnLongItemClickListener mLongItemClickListener;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<Data> getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.view.View getView(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    protected abstract int getItemResourceLayout();
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public abstract Holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType);
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    Holder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    public final void setOnItemClickListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.base.ui.adapter.BaseRecyclerAdapter.OnItemClickListener itemClickListener) {
    }
    
    public final void setOnLongItemClickListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.base.ui.adapter.BaseRecyclerAdapter.OnLongItemClickListener longItemClickListener) {
    }
    
    public final void add(Data item) {
    }
    
    public final void addAll(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends Data> items) {
    }
    
    public final void add(Data item, int position) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends Data> items) {
    }
    
    public final void addOrUpdate(Data item) {
    }
    
    public final void addOrUpdate(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends Data> items) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<Data> getData() {
        return null;
    }
    
    public final Data getData(int position) {
        return null;
    }
    
    public final void remove(int position) {
    }
    
    public final void remove(Data item) {
    }
    
    public final void clear() {
    }
    
    public BaseRecyclerAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter$OnItemClickListener;", "", "onItemClick", "", "view", "Landroid/view/View;", "position", "", "app_debug"})
    public static abstract interface OnItemClickListener {
        
        public abstract void onItemClick(@org.jetbrains.annotations.NotNull()
        android.view.View view, int position);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter$OnLongItemClickListener;", "", "onLongItemClick", "", "view", "Landroid/view/View;", "position", "", "app_debug"})
    public static abstract interface OnLongItemClickListener {
        
        public abstract void onLongItemClick(@org.jetbrains.annotations.NotNull()
        android.view.View view, int position);
    }
}