package com.suitcore.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"}, d2 = {"Lcom/suitcore/di/component/ApplicationComponent;", "", "inject", "", "eventPresenter", "Lcom/suitcore/feature/event/EventPresenter;", "guestPresenter", "Lcom/suitcore/feature/guest/GuestPresenter;", "loginPresenter", "Lcom/suitcore/feature/login/LoginPresenter;", "mainPresenter", "Lcom/suitcore/feature/main/MainPresenter;", "mapsPresenter", "Lcom/suitcore/feature/maps/MapsPresenter;", "splashScreenPresenter", "Lcom/suitcore/feature/splashscreen/SplashScreenPresenter;", "remoteConfigPresenter", "Lcom/suitcore/firebase/remoteconfig/RemoteConfigPresenter;", "oneSignalPresenter", "Lcom/suitcore/onesignal/OneSignalPresenter;", "app_debug"})
@dagger.Component(modules = {com.suitcore.di.module.ApplicationModule.class})
@com.suitcore.di.scope.SuitCoreApplicationScope()
public abstract interface ApplicationComponent {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.splashscreen.SplashScreenPresenter splashScreenPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.onesignal.OneSignalPresenter oneSignalPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.firebase.remoteconfig.RemoteConfigPresenter remoteConfigPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.event.EventPresenter eventPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.guest.GuestPresenter guestPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.main.MainPresenter mainPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.login.LoginPresenter loginPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.maps.MapsPresenter mapsPresenter);
}