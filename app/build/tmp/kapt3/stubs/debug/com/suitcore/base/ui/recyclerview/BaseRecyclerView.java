package com.suitcore.base.ui.recyclerview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b \u0018\u00002\u00020\u0001:\u0001nB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 J\u0016\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\tJ\b\u0010\"\u001a\u00020\u001bH\u0002J\b\u0010#\u001a\u00020\u001bH\u0002J\u0006\u0010$\u001a\u00020\u001bJ\u0006\u0010%\u001a\u00020\u001bJ\f\u0010&\u001a\b\u0012\u0002\b\u0003\u0018\u00010\'J\b\u0010(\u001a\u0004\u0018\u00010\u001dJ\b\u0010)\u001a\u0004\u0018\u00010\u001dJ\u0006\u0010*\u001a\u00020\u0015J\b\u0010+\u001a\u00020\u001bH\u0002J\u0006\u0010,\u001a\u00020\u001bJ\u0010\u0010-\u001a\u00020\u001b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010.\u001a\u00020\u001bH\u0002J\u0006\u0010/\u001a\u00020\u001bJ\u000e\u00100\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 J\u000e\u00101\u001a\u00020\u001b2\u0006\u00102\u001a\u00020\tJ\u0014\u00103\u001a\u00020\u001b2\f\u00104\u001a\b\u0012\u0002\b\u0003\u0018\u00010\'J\u000e\u00105\u001a\u00020\u001b2\u0006\u00106\u001a\u00020\tJ\u000e\u00107\u001a\u00020\u001b2\u0006\u00106\u001a\u00020\tJ\u000e\u00108\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010;\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010<\u001a\u00020\u001b2\u0006\u0010=\u001a\u00020>J\u000e\u0010?\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010?\u001a\u00020\u001b2\u0006\u0010@\u001a\u00020\tJ\u000e\u0010A\u001a\u00020\u001b2\u0006\u0010=\u001a\u00020>J\u000e\u0010B\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010B\u001a\u00020\u001b2\u0006\u0010@\u001a\u00020\tJ\u000e\u0010C\u001a\u00020\u001b2\u0006\u0010D\u001a\u00020\fJ\u000e\u0010E\u001a\u00020\u001b2\u0006\u0010F\u001a\u00020\tJ\u000e\u0010G\u001a\u00020\u001b2\u0006\u0010F\u001a\u00020\tJ\u000e\u0010H\u001a\u00020\u001b2\u0006\u0010I\u001a\u00020JJ\u000e\u0010K\u001a\u00020\u001b2\u0006\u0010L\u001a\u00020MJ\u000e\u0010N\u001a\u00020\u001b2\u0006\u0010=\u001a\u00020OJ\u000e\u0010P\u001a\u00020\u001b2\u0006\u0010Q\u001a\u00020\fJ\u000e\u0010R\u001a\u00020\u001b2\u0006\u0010S\u001a\u00020\fJ\u000e\u0010T\u001a\u00020\u001b2\u0006\u0010U\u001a\u00020\fJ\u000e\u0010V\u001a\u00020\u001b2\u0006\u0010W\u001a\u00020\fJ&\u0010X\u001a\u00020\u001b2\u0006\u0010Y\u001a\u00020\t2\u0006\u0010Z\u001a\u00020\t2\u0006\u0010[\u001a\u00020\t2\u0006\u0010\\\u001a\u00020\tJ\u000e\u0010]\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010^\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010_\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010`\u001a\u00020\u001b2\u0006\u00109\u001a\u00020:J\u000e\u0010a\u001a\u00020\u001b2\u0006\u0010b\u001a\u00020\tJ\u000e\u0010c\u001a\u00020\u001b2\u0006\u0010b\u001a\u00020\tJ\u0006\u0010d\u001a\u00020\u001bJ\u0006\u0010e\u001a\u00020\u001bJ\u0006\u0010f\u001a\u00020\u001bJ\u000e\u0010g\u001a\u00020\u001b2\u0006\u0010h\u001a\u00020\fJ\u0006\u0010i\u001a\u00020\u001bJ\u000e\u0010j\u001a\u00020\u001b2\u0006\u0010h\u001a\u00020\fJ\u0006\u0010k\u001a\u00020\u001bJ\u0006\u0010l\u001a\u00020\u001bJ\u0006\u0010m\u001a\u00020\u001bR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006o"}, d2 = {"Lcom/suitcore/base/ui/recyclerview/BaseRecyclerView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "mClipToPadding", "", "mEmptyView", "mErrorView", "mPadding", "mPaddingBottom", "mPaddingLeft", "mPaddingRight", "mPaddingTop", "mRecyclerView", "Lcom/jcodecraeer/xrecyclerview/XRecyclerView;", "mScrollbar", "mScrollbarStyle", "mShimmerContainer", "Lcom/facebook/shimmer/ShimmerFrameLayout;", "addHeaderView", "", "view", "Landroid/view/View;", "addItemDecoration", "decor", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "index", "addViews", "buildViews", "completeRefresh", "destroy", "getAdapter", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "getEmptyView", "getErrorView", "getRecyclerView", "hideAll", "hideEmpty", "initAttrs", "initView", "loadMoreComplete", "removeItemDecoration", "scrollToPosition", "position", "setAdapter", "adapter", "setBackgroungEmptyButton", "drawableRes", "setBackgroungErrorButton", "setContentEmptyView", "text", "", "setContentErrorView", "setEmptyButtonListener", "listener", "Lcom/suitcore/base/ui/recyclerview/BaseRecyclerView$ReloadListener;", "setEmptyView", "viewResourceId", "setErrorButtonListener", "setErrorView", "setHasFixedSize", "hasFixedSize", "setImageEmptyView", "imageRes", "setImageErrorView", "setItemAnimator", "animator", "Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;", "setLayoutManager", "layout", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "setLoadingListener", "Lcom/jcodecraeer/xrecyclerview/XRecyclerView$LoadingListener;", "setLoadingMoreEnabled", "isLoadMore", "setNoMore", "isNoMore", "setPullToRefreshEnable", "isPullToRefresh", "setRecyclerClipToPadding", "clipToPadding", "setRecyclerViewPadding", "left", "top", "right", "bottom", "setTextButtonEmptyView", "setTextButtonErrorView", "setTitleEmptyView", "setTitleErrorView", "setUpAsGrid", "spanCount", "setUpAsGridInScroll", "setUpAsList", "setUpAsListInScroll", "showEmpty", "showEmptyTitleView", "status", "showError", "showErrorTitleView", "showRecycler", "showShimmer", "stopShimmer", "ReloadListener", "app_debug"})
public final class BaseRecyclerView extends android.widget.FrameLayout {
    private com.jcodecraeer.xrecyclerview.XRecyclerView mRecyclerView;
    private android.widget.FrameLayout mEmptyView;
    private android.widget.FrameLayout mErrorView;
    private com.facebook.shimmer.ShimmerFrameLayout mShimmerContainer;
    private boolean mClipToPadding;
    private int mPadding;
    private int mPaddingTop;
    private int mPaddingBottom;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mScrollbarStyle;
    private int mScrollbar;
    private java.util.HashMap _$_findViewCache;
    
    private final void initAttrs(android.util.AttributeSet attrs) {
    }
    
    private final void initView() {
    }
    
    private final void buildViews() {
    }
    
    private final void addViews() {
    }
    
    public final void setUpAsList() {
    }
    
    public final void setUpAsListInScroll() {
    }
    
    public final void setUpAsGrid(int spanCount) {
    }
    
    public final void setUpAsGridInScroll(int spanCount) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.jcodecraeer.xrecyclerview.XRecyclerView getRecyclerView() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.view.View getEmptyView() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.view.View getErrorView() {
        return null;
    }
    
    public final void setEmptyView(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void setEmptyView(int viewResourceId) {
    }
    
    public final void setErrorView(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void setErrorView(int viewResourceId) {
    }
    
    /**
     * Below are some methods for setting the RecyclerView attributes
     */
    public final void setRecyclerViewPadding(int left, int top, int right, int bottom) {
    }
    
    public final void setRecyclerClipToPadding(boolean clipToPadding) {
    }
    
    public final void setHasFixedSize(boolean hasFixedSize) {
    }
    
    public final void setItemAnimator(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ItemAnimator animator) {
    }
    
    public final void addItemDecoration(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ItemDecoration decor) {
    }
    
    public final void addItemDecoration(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ItemDecoration decor, int index) {
    }
    
    public final void removeItemDecoration(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ItemDecoration decor) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView.Adapter<?> getAdapter() {
        return null;
    }
    
    /**
     * Below are some methods from XRecyclerView
     */
    public final void destroy() {
    }
    
    public final void addHeaderView(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void loadMoreComplete() {
    }
    
    public final void setNoMore(boolean isNoMore) {
    }
    
    public final void setPullToRefreshEnable(boolean isPullToRefresh) {
    }
    
    public final void completeRefresh() {
    }
    
    public final void setLoadingMoreEnabled(boolean isLoadMore) {
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView.Adapter<?> adapter) {
    }
    
    public final void setLayoutManager(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.LayoutManager layout) {
    }
    
    public final void setLoadingListener(@org.jetbrains.annotations.NotNull()
    com.jcodecraeer.xrecyclerview.XRecyclerView.LoadingListener listener) {
    }
    
    public final void scrollToPosition(int position) {
    }
    
    /**
     * Below are some methods for managing layout visibility
     */
    private final void hideAll() {
    }
    
    public final void showRecycler() {
    }
    
    public final void showShimmer() {
    }
    
    public final void stopShimmer() {
    }
    
    public final void showEmpty() {
    }
    
    public final void hideEmpty() {
    }
    
    public final void showError() {
    }
    
    /**
     * Below are some methods for managing empty state & error state
     */
    public final void setImageEmptyView(int imageRes) {
    }
    
    public final void setTitleEmptyView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void setContentEmptyView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void showEmptyTitleView(boolean status) {
    }
    
    public final void setTextButtonEmptyView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void setBackgroungEmptyButton(int drawableRes) {
    }
    
    public final void setEmptyButtonListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView.ReloadListener listener) {
    }
    
    public final void setImageErrorView(int imageRes) {
    }
    
    public final void setTitleErrorView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void setContentErrorView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void showErrorTitleView(boolean status) {
    }
    
    public final void setTextButtonErrorView(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void setBackgroungErrorButton(int drawableRes) {
    }
    
    public final void setErrorButtonListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView.ReloadListener listener) {
    }
    
    public BaseRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    public BaseRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    public BaseRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs, int defStyle) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/suitcore/base/ui/recyclerview/BaseRecyclerView$ReloadListener;", "Landroid/view/View$OnClickListener;", "app_debug"})
    public static abstract interface ReloadListener extends android.view.View.OnClickListener {
    }
}