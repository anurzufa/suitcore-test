package com.suitcore.data.remote.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\b\u0010\u0007\u001a\u00020\bH\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/suitcore/data/remote/services/BaseServiceFactory;", "", "()V", "SERVICE_TAG", "", "getAPIService", "Lcom/suitcore/data/remote/services/APIService;", "makeGSON", "Lcom/google/gson/Gson;", "provideOkHttpClient", "Lokhttp3/OkHttpClient;", "provideRetrofit", "gSon", "app_debug"})
public final class BaseServiceFactory {
    private static java.lang.String SERVICE_TAG;
    public static final com.suitcore.data.remote.services.BaseServiceFactory INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.suitcore.data.remote.services.APIService getAPIService() {
        return null;
    }
    
    private final com.suitcore.data.remote.services.APIService provideRetrofit(com.google.gson.Gson gSon) {
        return null;
    }
    
    private final okhttp3.OkHttpClient provideOkHttpClient() {
        return null;
    }
    
    private final com.google.gson.Gson makeGSON() {
        return null;
    }
    
    private BaseServiceFactory() {
        super();
    }
}