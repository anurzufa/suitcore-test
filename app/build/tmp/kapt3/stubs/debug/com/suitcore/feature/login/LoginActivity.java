package com.suitcore.feature.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\u0012\u0010\f\u001a\u00020\u000b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u000bH\u0002J\b\u0010\u0010\u001a\u00020\u000bH\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006\u0011"}, d2 = {"Lcom/suitcore/feature/login/LoginActivity;", "Lcom/suitcore/base/ui/BaseActivity;", "Lcom/suitcore/feature/login/LoginView;", "()V", "loginPresenter", "Lcom/suitcore/feature/login/LoginPresenter;", "resourceLayout", "", "getResourceLayout", "()I", "isEmptyField", "", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setupPresenter", "startNextActivity", "app_debug"})
public final class LoginActivity extends com.suitcore.base.ui.BaseActivity implements com.suitcore.feature.login.LoginView {
    private com.suitcore.feature.login.LoginPresenter loginPresenter;
    private final int resourceLayout = com.suitcore.R.layout.activity_first;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupPresenter() {
    }
    
    @java.lang.Override()
    public void startNextActivity() {
    }
    
    @java.lang.Override()
    public void isEmptyField() {
    }
    
    public LoginActivity() {
        super();
    }
}