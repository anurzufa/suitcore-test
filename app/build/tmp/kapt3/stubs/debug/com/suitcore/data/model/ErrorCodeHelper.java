package com.suitcore.data.model;

import java.lang.System;

/**
 * Created by dodydmw19 on 2/12/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/suitcore/data/model/ErrorCodeHelper;", "", "()V", "Companion", "app_debug"})
public final class ErrorCodeHelper {
    public static final com.suitcore.data.model.ErrorCodeHelper.Companion Companion = null;
    
    public ErrorCodeHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0001\u00a8\u0006\b"}, d2 = {"Lcom/suitcore/data/model/ErrorCodeHelper$Companion;", "", "()V", "getErrorMessage", "", "context", "Landroid/content/Context;", "e", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getErrorMessage(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.Object e) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}