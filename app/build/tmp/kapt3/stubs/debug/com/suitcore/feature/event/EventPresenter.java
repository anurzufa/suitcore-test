package com.suitcore.feature.event;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0016J\b\u0010\u0015\u001a\u00020\u0013H\u0016J\u0006\u0010\u0016\u001a\u00020\u0013J\u0006\u0010\u0017\u001a\u00020\u0013J\u0010\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bH\u0002J\b\u0010\u0019\u001a\u00020\u0013H\u0016J\u0016\u0010\u001a\u001a\u00020\u00132\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0002R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/suitcore/feature/event/EventPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/feature/event/EventView;", "()V", "apiService", "Lcom/suitcore/data/remote/services/APIService;", "getApiService", "()Lcom/suitcore/data/remote/services/APIService;", "setApiService", "(Lcom/suitcore/data/remote/services/APIService;)V", "arrayData", "", "Lcom/suitcore/data/model/Event;", "mCompositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "mRealm", "Lcom/suitcore/data/local/RealmHelper;", "mvpView", "attachView", "", "view", "detachView", "getEvent", "getEventCache", "mEventList", "onDestroy", "saveToCache", "data", "app_debug"})
public final class EventPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.feature.event.EventView> {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.suitcore.data.remote.services.APIService apiService;
    private com.suitcore.feature.event.EventView mvpView;
    private com.suitcore.data.local.RealmHelper<com.suitcore.data.model.Event> mRealm;
    private io.reactivex.disposables.CompositeDisposable mCompositeDisposable;
    private final java.util.List<com.suitcore.data.model.Event> arrayData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.suitcore.data.remote.services.APIService getApiService() {
        return null;
    }
    
    public final void setApiService(@org.jetbrains.annotations.NotNull()
    com.suitcore.data.remote.services.APIService p0) {
    }
    
    public final void getEventCache() {
    }
    
    private final java.util.List<com.suitcore.data.model.Event> mEventList() {
        return null;
    }
    
    public final void getEvent() {
    }
    
    private final void saveToCache(java.util.List<? extends com.suitcore.data.model.Event> data) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.event.EventView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public EventPresenter() {
        super();
    }
}