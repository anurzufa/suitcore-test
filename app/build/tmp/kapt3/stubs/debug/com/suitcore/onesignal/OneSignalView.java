package com.suitcore.onesignal;

import java.lang.System;

/**
 * Created by dodydmw19 on 6/12/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0012\u0010\u0006\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\bH&\u00a8\u0006\t"}, d2 = {"Lcom/suitcore/onesignal/OneSignalView;", "Lcom/suitcore/base/presenter/MvpView;", "onRegisterIdFailed", "", "error", "", "onRegisterIdSuccess", "message", "", "app_debug"})
public abstract interface OneSignalView extends com.suitcore.base.presenter.MvpView {
    
    public abstract void onRegisterIdSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.String message);
    
    public abstract void onRegisterIdFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error);
}