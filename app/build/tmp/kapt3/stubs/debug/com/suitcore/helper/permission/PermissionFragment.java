package com.suitcore.helper.permission;

import java.lang.System;

/**
 * WARNING! DO NOT USE THIS FRAGMENT DIRECTLY!
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\t\u0018\u0000 \u001d2\u00020\u0001:\u0003\u001d\u001e\u001fB\u0005\u00a2\u0006\u0002\u0010\u0002J#\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00042\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0000\u00a2\u0006\u0002\b\u0010J+\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u0019\u001a\u00020\u000eH\u0016J\b\u0010\u001a\u001a\u00020\u000eH\u0002J\b\u0010\u001b\u001a\u00020\u000eH\u0002J\b\u0010\u001c\u001a\u00020\u000eH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "}, d2 = {"Lcom/suitcore/helper/permission/PermissionFragment;", "Landroidx/fragment/app/Fragment;", "()V", "listener", "Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;", "permissionQueue", "Ljava/util/concurrent/ConcurrentLinkedQueue;", "Lcom/suitcore/helper/permission/PermissionFragment$PermissionHolder;", "permissionsList", "", "", "waitingForReceive", "", "addPermissionForRequest", "", "permission", "addPermissionForRequest$app_debug", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "proceedPermissions", "removeFragment", "runQueuePermission", "Companion", "PermissionHolder", "PermissionListener", "app_debug"})
public final class PermissionFragment extends androidx.fragment.app.Fragment {
    private final java.util.concurrent.ConcurrentLinkedQueue<com.suitcore.helper.permission.PermissionFragment.PermissionHolder> permissionQueue = null;
    private java.util.List<java.lang.String> permissionsList;
    private com.suitcore.helper.permission.PermissionFragment.PermissionListener listener;
    private boolean waitingForReceive;
    private static final int REQUEST_CODE = 23000;
    private static final java.lang.String TAG = "PermissionFragment";
    public static final com.suitcore.helper.permission.PermissionFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onResume() {
    }
    
    private final void runQueuePermission() {
    }
    
    private final void proceedPermissions() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void removeFragment() {
    }
    
    public final void addPermissionForRequest$app_debug(@org.jetbrains.annotations.NotNull()
    com.suitcore.helper.permission.PermissionFragment.PermissionListener listener, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permission) {
    }
    
    public PermissionFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001J2\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006\t"}, d2 = {"Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;", "", "onRequestPermissionsResult", "", "acceptedPermissions", "", "", "refusedPermissions", "askAgainPermissions", "app_debug"})
    public static abstract interface PermissionListener {
        
        public abstract void onRequestPermissionsResult(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> acceptedPermissions, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> refusedPermissions, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> askAgainPermissions);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0015"}, d2 = {"Lcom/suitcore/helper/permission/PermissionFragment$PermissionHolder;", "", "permissions", "", "", "listener", "Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;", "(Ljava/util/List;Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;)V", "getListener", "()Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;", "getPermissions", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
    static final class PermissionHolder {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> permissions = null;
        @org.jetbrains.annotations.NotNull()
        private final com.suitcore.helper.permission.PermissionFragment.PermissionListener listener = null;
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getPermissions() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.PermissionFragment.PermissionListener getListener() {
            return null;
        }
        
        public PermissionHolder(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
        com.suitcore.helper.permission.PermissionFragment.PermissionListener listener) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.PermissionFragment.PermissionListener component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.PermissionFragment.PermissionHolder copy(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
        com.suitcore.helper.permission.PermissionFragment.PermissionListener listener) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object p0) {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/suitcore/helper/permission/PermissionFragment$Companion;", "", "()V", "REQUEST_CODE", "", "TAG", "", "newInstance", "Lcom/suitcore/helper/permission/PermissionFragment;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.PermissionFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}