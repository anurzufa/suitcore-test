package com.suitcore.firebase.remoteconfig;

import java.lang.System;

/**
 * Created by dodydmw19 on 9/27/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/suitcore/firebase/remoteconfig/RemoteConfigHelper;", "", "()V", "Companion", "app_debug"})
public final class RemoteConfigHelper {
    public static final com.suitcore.firebase.remoteconfig.RemoteConfigHelper.Companion Companion = null;
    
    public RemoteConfigHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b\u00a8\u0006\n"}, d2 = {"Lcom/suitcore/firebase/remoteconfig/RemoteConfigHelper$Companion;", "", "()V", "changeBaseUrl", "", "activity", "Landroid/app/Activity;", "type", "", "endpoint", "app_debug"})
    public static final class Companion {
        
        public final void changeBaseUrl(@org.jetbrains.annotations.Nullable()
        android.app.Activity activity, @org.jetbrains.annotations.NotNull()
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint) {
        }
        
        private Companion() {
            super();
        }
    }
}