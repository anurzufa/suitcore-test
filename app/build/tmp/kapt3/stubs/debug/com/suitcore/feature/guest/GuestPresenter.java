package com.suitcore.feature.guest;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0016J\b\u0010\u0013\u001a\u00020\u0011H\u0016J\u0006\u0010\u0014\u001a\u00020\u0011J\u0006\u0010\u0015\u001a\u00020\u0011J\b\u0010\u0016\u001a\u00020\u0011H\u0016J\u0016\u0010\u0017\u001a\u00020\u00112\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0019H\u0002R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/suitcore/feature/guest/GuestPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/feature/guest/GuestView;", "()V", "apiService", "Lcom/suitcore/data/remote/services/APIService;", "getApiService", "()Lcom/suitcore/data/remote/services/APIService;", "setApiService", "(Lcom/suitcore/data/remote/services/APIService;)V", "mCompositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "mRealm", "Lcom/suitcore/data/local/RealmHelper;", "Lcom/suitcore/data/model/Guest;", "mvpView", "attachView", "", "view", "detachView", "getGuest", "getGuestCache", "onDestroy", "saveToCache", "data", "", "app_debug"})
public final class GuestPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.feature.guest.GuestView> {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.suitcore.data.remote.services.APIService apiService;
    private com.suitcore.feature.guest.GuestView mvpView;
    private com.suitcore.data.local.RealmHelper<com.suitcore.data.model.Guest> mRealm;
    private io.reactivex.disposables.CompositeDisposable mCompositeDisposable;
    
    @org.jetbrains.annotations.NotNull()
    public final com.suitcore.data.remote.services.APIService getApiService() {
        return null;
    }
    
    public final void setApiService(@org.jetbrains.annotations.NotNull()
    com.suitcore.data.remote.services.APIService p0) {
    }
    
    public final void getGuestCache() {
    }
    
    public final void getGuest() {
    }
    
    private final void saveToCache(java.util.List<? extends com.suitcore.data.model.Guest> data) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.guest.GuestView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public GuestPresenter() {
        super();
    }
}