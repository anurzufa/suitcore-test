package com.suitcore.helper.permission;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/suitcore/helper/permission/SuitPermissions;", "", "()V", "TAG", "", "semaphore", "Ljava/util/concurrent/Semaphore;", "with", "Lcom/suitcore/helper/permission/SuitPermissions$PermissionCore;", "activity", "Landroidx/fragment/app/FragmentActivity;", "PermissionCore", "app_debug"})
public final class SuitPermissions {
    private static final java.lang.String TAG = "KotlinPermission";
    private static final java.util.concurrent.Semaphore semaphore = null;
    public static final com.suitcore.helper.permission.SuitPermissions INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.suitcore.helper.permission.SuitPermissions.PermissionCore with(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity activity) {
        return null;
    }
    
    private SuitPermissions() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0010\u001a\u00020\u0011J \u0010\u0012\u001a\u00020\u00002\u0018\u0010\u0013\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0004\u0012\u00020\u00110\u0014J\u000e\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0006J\u0016\u0010\u0015\u001a\u00020\u00112\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J \u0010\u0016\u001a\u00020\u00002\u0018\u0010\u0013\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0004\u0012\u00020\u00110\u0014J\u000e\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0006J \u0010\u0017\u001a\u00020\u00002\u0018\u0010\u0013\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0004\u0012\u00020\u00110\u0014J\u000e\u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0006J=\u0010\u0018\u001a\u00020\u00112\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000eH\u0000\u00a2\u0006\u0002\b\u001cJ\u001e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J\u001f\u0010\r\u001a\u00020\u00002\u0012\u0010!\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000f0\"\"\u00020\u000f\u00a2\u0006\u0002\u0010#R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/suitcore/helper/permission/SuitPermissions$PermissionCore;", "", "activity", "Landroidx/fragment/app/FragmentActivity;", "(Landroidx/fragment/app/FragmentActivity;)V", "acceptedCallback", "Lcom/suitcore/helper/permission/ResponsePermissionCallback;", "activityReference", "Ljava/lang/ref/WeakReference;", "deniedCallback", "foreverDeniedCallback", "listener", "Lcom/suitcore/helper/permission/PermissionFragment$PermissionListener;", "permissions", "", "", "ask", "", "onAccepted", "callback", "Lkotlin/Function1;", "onAcceptedPermission", "onDenied", "onForeverDenied", "onReceivedPermissionResult", "acceptedPermissions", "foreverDenied", "denied", "onReceivedPermissionResult$app_debug", "permissionAlreadyAccepted", "", "context", "Landroid/content/Context;", "permission", "", "([Ljava/lang/String;)Lcom/suitcore/helper/permission/SuitPermissions$PermissionCore;", "app_debug"})
    public static final class PermissionCore {
        private final java.lang.ref.WeakReference<androidx.fragment.app.FragmentActivity> activityReference = null;
        private java.util.List<java.lang.String> permissions;
        private com.suitcore.helper.permission.ResponsePermissionCallback acceptedCallback;
        private com.suitcore.helper.permission.ResponsePermissionCallback deniedCallback;
        private com.suitcore.helper.permission.ResponsePermissionCallback foreverDeniedCallback;
        private final com.suitcore.helper.permission.PermissionFragment.PermissionListener listener = null;
        
        public final void onReceivedPermissionResult$app_debug(@org.jetbrains.annotations.Nullable()
        java.util.List<java.lang.String> acceptedPermissions, @org.jetbrains.annotations.Nullable()
        java.util.List<java.lang.String> foreverDenied, @org.jetbrains.annotations.Nullable()
        java.util.List<java.lang.String> denied) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore permissions(@org.jetbrains.annotations.NotNull()
        java.lang.String... permission) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onAccepted(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.util.List<java.lang.String>, kotlin.Unit> callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onAccepted(@org.jetbrains.annotations.NotNull()
        com.suitcore.helper.permission.ResponsePermissionCallback callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onDenied(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.util.List<java.lang.String>, kotlin.Unit> callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onDenied(@org.jetbrains.annotations.NotNull()
        com.suitcore.helper.permission.ResponsePermissionCallback callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onForeverDenied(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.util.List<java.lang.String>, kotlin.Unit> callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.permission.SuitPermissions.PermissionCore onForeverDenied(@org.jetbrains.annotations.NotNull()
        com.suitcore.helper.permission.ResponsePermissionCallback callback) {
            return null;
        }
        
        public final void ask() {
        }
        
        private final void onAcceptedPermission(java.util.List<java.lang.String> permissions) {
        }
        
        private final boolean permissionAlreadyAccepted(android.content.Context context, java.util.List<java.lang.String> permissions) {
            return false;
        }
        
        public PermissionCore(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.FragmentActivity activity) {
            super();
        }
    }
}