package com.suitcore.base.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b&\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013J\u0010\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0013J0\u0010\u0016\u001a\u00020\u00112\u000e\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00190\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001dJ(\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\r2\u000e\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00190\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bJ\b\u0010 \u001a\u00020\u0011H\u0016J\u0012\u0010!\u001a\u00020\u00112\b\u0010\"\u001a\u0004\u0018\u00010\u001bH\u0016J\u0012\u0010#\u001a\u00020\u00112\b\u0010\"\u001a\u0004\u0018\u00010\u001bH\u0016J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\u000b2\b\u0010\'\u001a\u0004\u0018\u00010(2\b\u0010\"\u001a\u0004\u0018\u00010\u001bH\u0016J\u0012\u0010)\u001a\u00020\u00112\b\u0010\"\u001a\u0004\u0018\u00010\u001bH$J\u0010\u0010*\u001a\u00020\u00112\u0006\u0010+\u001a\u00020\rH\u0016J\u0010\u0010*\u001a\u00020\u00112\u0006\u0010+\u001a\u00020,H\u0016J\u001e\u0010-\u001a\u00020\u00112\u0006\u0010+\u001a\u00020\r2\f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00110/H\u0016J\u001e\u0010-\u001a\u00020\u00112\u0006\u0010+\u001a\u00020,2\f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00110/H\u0016J\u001e\u00100\u001a\u00020\u00112\u0006\u0010+\u001a\u00020,2\f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00110/H\u0016J)\u00101\u001a\u00020\u00112\u0006\u00102\u001a\u00020\u001d2\b\u0010+\u001a\u0004\u0018\u00010,2\b\u00103\u001a\u0004\u0018\u00010\rH\u0016\u00a2\u0006\u0002\u00104J\u0010\u00105\u001a\u00020\u00112\u0006\u00106\u001a\u00020\rH\u0016J\u0010\u00105\u001a\u00020\u00112\u0006\u00106\u001a\u00020,H\u0016J\u0010\u00107\u001a\u00020\u00112\u0006\u0010+\u001a\u00020,H\u0004R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\f\u001a\u00020\rX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f\u00a8\u00068"}, d2 = {"Lcom/suitcore/base/ui/BaseFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/suitcore/base/presenter/MvpView;", "()V", "mBaseActivity", "Lcom/suitcore/base/ui/BaseActivity;", "mCommonLoadingDialog", "Lcom/suitcore/helper/CommonLoadingDialog;", "mContext", "Landroid/content/Context;", "mInflater", "Landroid/view/LayoutInflater;", "resourceLayout", "", "getResourceLayout", "()I", "clearRecyclerView", "", "recyclerView", "Lcom/suitcore/base/ui/recyclerview/BaseRecyclerView;", "finishLoad", "recycler", "goToActivity", "actDestination", "Ljava/lang/Class;", "Landroid/app/Activity;", "data", "Landroid/os/Bundle;", "clearIntent", "", "isFinish", "resultCode", "hideLoading", "onActivityCreated", "savedInstanceState", "onCreate", "onCreateView", "Landroid/view/View;", "inflater", "container", "Landroid/view/ViewGroup;", "onViewReady", "showAlertDialog", "message", "", "showConfirmationDialog", "confirmCallback", "Lkotlin/Function0;", "showConfirmationSingleDialog", "showLoading", "isBackPressedCancelable", "currentPage", "(ZLjava/lang/String;Ljava/lang/Integer;)V", "showLoadingWithText", "msg", "showToast", "app_debug"})
public abstract class BaseFragment extends androidx.fragment.app.Fragment implements com.suitcore.base.presenter.MvpView {
    private android.content.Context mContext;
    private com.suitcore.base.ui.BaseActivity mBaseActivity;
    private android.view.LayoutInflater mInflater;
    private com.suitcore.helper.CommonLoadingDialog mCommonLoadingDialog;
    private java.util.HashMap _$_findViewCache;
    
    protected abstract int getResourceLayout();
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    protected abstract void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState);
    
    protected final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void goToActivity(@org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends android.app.Activity> actDestination, @org.jetbrains.annotations.Nullable()
    android.os.Bundle data, boolean clearIntent, boolean isFinish) {
    }
    
    public final void goToActivity(int resultCode, @org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends android.app.Activity> actDestination, @org.jetbrains.annotations.Nullable()
    android.os.Bundle data) {
    }
    
    @java.lang.Override()
    public void showLoading(boolean isBackPressedCancelable, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Integer currentPage) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(int msg) {
    }
    
    @java.lang.Override()
    public void hideLoading() {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationSingleDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(int message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(int message) {
    }
    
    public final void finishLoad(@org.jetbrains.annotations.Nullable()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView recycler) {
    }
    
    public final void clearRecyclerView(@org.jetbrains.annotations.Nullable()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView recyclerView) {
    }
    
    public BaseFragment() {
        super();
    }
}