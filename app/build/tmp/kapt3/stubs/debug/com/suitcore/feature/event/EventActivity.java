package com.suitcore.feature.event;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\u0012\u0010\u0012\u001a\u00020\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0011H\u0014J\u0012\u0010\u0016\u001a\u00020\u00112\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0018\u0010\u0019\u001a\u00020\u00112\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u0011H\u0016J\u0018\u0010\u001d\u001a\u00020\u00112\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u001bH\u0016J\u0012\u0010\u001e\u001a\u00020\u00112\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0018\u0010!\u001a\u00020\u00112\u000e\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u001bH\u0002J\b\u0010#\u001a\u00020\u0011H\u0002J\b\u0010$\u001a\u00020\u0011H\u0002J\b\u0010%\u001a\u00020\u0011H\u0002J\b\u0010&\u001a\u00020\u0011H\u0002J\b\u0010\'\u001a\u00020\u0011H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\rX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006("}, d2 = {"Lcom/suitcore/feature/event/EventActivity;", "Lcom/suitcore/base/ui/BaseActivity;", "Lcom/suitcore/feature/event/EventView;", "Lcom/suitcore/feature/event/EventItemView$OnActionListener;", "()V", "eventAdapter", "Lcom/suitcore/feature/event/EventAdapter;", "eventPresenter", "Lcom/suitcore/feature/event/EventPresenter;", "realmHelper", "Lcom/suitcore/data/local/RealmHelper;", "Lcom/suitcore/data/model/Event;", "resourceLayout", "", "getResourceLayout", "()I", "loadData", "", "onClicked", "view", "Lcom/suitcore/feature/event/EventItemView;", "onDestroy", "onFailed", "error", "", "onMemberCacheLoaded", "event", "", "onMemberEmpty", "onMemberLoaded", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setData", "data", "setupEmptyView", "setupErrorView", "setupList", "setupPresenter", "setupProgressView", "app_debug"})
public final class EventActivity extends com.suitcore.base.ui.BaseActivity implements com.suitcore.feature.event.EventView, com.suitcore.feature.event.EventItemView.OnActionListener {
    private com.suitcore.feature.event.EventPresenter eventPresenter;
    private com.suitcore.feature.event.EventAdapter eventAdapter;
    private com.suitcore.data.local.RealmHelper<com.suitcore.data.model.Event> realmHelper;
    private final int resourceLayout = com.suitcore.R.layout.activity_event;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void setupPresenter() {
    }
    
    private final void setupList() {
    }
    
    private final void loadData() {
    }
    
    private final void setData(java.util.List<? extends com.suitcore.data.model.Event> data) {
    }
    
    private final void setupProgressView() {
    }
    
    private final void setupEmptyView() {
    }
    
    private final void setupErrorView() {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.Nullable()
    com.suitcore.feature.event.EventItemView view) {
    }
    
    @java.lang.Override()
    public void onMemberCacheLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Event> event) {
    }
    
    @java.lang.Override()
    public void onMemberLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Event> event) {
    }
    
    @java.lang.Override()
    public void onMemberEmpty() {
    }
    
    @java.lang.Override()
    public void onFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error) {
    }
    
    public EventActivity() {
        super();
    }
}