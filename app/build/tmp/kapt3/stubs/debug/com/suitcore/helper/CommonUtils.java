package com.suitcore.helper;

import java.lang.System;

/**
 * Created by dodydmw19 on 7/18/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/suitcore/helper/CommonUtils;", "", "()V", "Companion", "app_debug"})
public final class CommonUtils {
    public static final com.suitcore.helper.CommonUtils.Companion Companion = null;
    
    public CommonUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\bJ\u001e\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0\fJ\u000e\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u000f\u001a\u00020\b2\b\u0010\u0010\u001a\u0004\u0018\u00010\rJ\u0010\u0010\u000f\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0006\u0010\u0011\u001a\u00020\b\u00a8\u0006\u0012"}, d2 = {"Lcom/suitcore/helper/CommonUtils$Companion;", "", "()V", "checkTwitterApp", "", "context", "Landroid/content/Context;", "clearLocalStorage", "", "createIntent", "Landroid/content/Intent;", "actDestination", "Ljava/lang/Class;", "Landroid/app/Activity;", "openAppInStore", "restartApp", "activity", "setDefaultBaseUrlIfNeeded", "app_debug"})
    public static final class Companion {
        
        public final boolean checkTwitterApp(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return false;
        }
        
        public final void openAppInStore(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void restartApp(@org.jetbrains.annotations.Nullable()
        android.app.Activity activity) {
        }
        
        public final void restartApp(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
        }
        
        public final void clearLocalStorage() {
        }
        
        public final void setDefaultBaseUrlIfNeeded() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent createIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.Class<? extends android.app.Activity> actDestination) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}