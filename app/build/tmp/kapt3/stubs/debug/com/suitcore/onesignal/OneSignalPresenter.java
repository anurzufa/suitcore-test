package com.suitcore.onesignal;

import java.lang.System;

/**
 * Created by dodydmw19 on 6/12/19.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016J\b\u0010\u0010\u001a\u00020\u000eH\u0016J\b\u0010\u0011\u001a\u00020\u000eH\u0016J\u000e\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/suitcore/onesignal/OneSignalPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/onesignal/OneSignalView;", "()V", "apiService", "Lcom/suitcore/data/remote/services/APIService;", "getApiService", "()Lcom/suitcore/data/remote/services/APIService;", "setApiService", "(Lcom/suitcore/data/remote/services/APIService;)V", "mCompositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "mvpView", "attachView", "", "view", "detachView", "onDestroy", "registerPlayerId", "playerId", "", "app_debug"})
public final class OneSignalPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.onesignal.OneSignalView> {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.suitcore.data.remote.services.APIService apiService;
    private com.suitcore.onesignal.OneSignalView mvpView;
    private io.reactivex.disposables.CompositeDisposable mCompositeDisposable;
    
    @org.jetbrains.annotations.NotNull()
    public final com.suitcore.data.remote.services.APIService getApiService() {
        return null;
    }
    
    public final void setApiService(@org.jetbrains.annotations.NotNull()
    com.suitcore.data.remote.services.APIService p0) {
    }
    
    public final void registerPlayerId(@org.jetbrains.annotations.NotNull()
    java.lang.String playerId) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.onesignal.OneSignalView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public OneSignalPresenter() {
        super();
    }
}