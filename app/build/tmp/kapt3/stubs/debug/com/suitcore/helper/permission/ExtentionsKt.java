package com.suitcore.helper.permission;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000.\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u001aF\u0010\u0000\u001a\u0002H\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003\"\u0004\b\u0001\u0010\u0001*\u0004\u0018\u0001H\u00022\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0007H\u0086\b\u00a2\u0006\u0002\u0010\b\u001a.\u0010\t\u001a\u00020\n\"\u0004\b\u0000\u0010\u0002*\u0004\u0018\u0001H\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\n0\u0005H\u0086\b\u00a2\u0006\u0002\u0010\f\u001aB\u0010\r\u001a\u00020\n\"\b\b\u0000\u0010\u000e*\u00020\u0003\"\u000e\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u000e0\u000f*\u0004\u0018\u0001H\u00022\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\n0\u0005H\u0086\b\u00a2\u0006\u0002\u0010\u0011\u001aG\u0010\u0012\u001a\u00020\n\"\b\b\u0000\u0010\u000e*\u00020\u0003\"\u000e\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u000e0\u000f*\u0004\u0018\u0001H\u00022\u0017\u0010\u0010\u001a\u0013\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\u0002\b\u0013H\u0086\b\u00a2\u0006\u0002\u0010\u0011\u00a8\u0006\u0014"}, d2 = {"ifNotNullOrElse", "R", "T", "", "ifNotNullPath", "Lkotlin/Function1;", "elsePath", "Lkotlin/Function0;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "notNull", "", "f", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "whenNotNullNorEmpty", "E", "", "func", "(Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)V", "withNotNullNorEmpty", "Lkotlin/ExtensionFunctionType;", "app_debug"})
public final class ExtentionsKt {
    
    public static final <E extends java.lang.Object, T extends java.util.Collection<? extends E>>void withNotNullNorEmpty(@org.jetbrains.annotations.Nullable()
    T $this$withNotNullNorEmpty, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, kotlin.Unit> func) {
    }
    
    public static final <E extends java.lang.Object, T extends java.util.Collection<? extends E>>void whenNotNullNorEmpty(@org.jetbrains.annotations.Nullable()
    T $this$whenNotNullNorEmpty, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, kotlin.Unit> func) {
    }
    
    public static final <T extends java.lang.Object>void notNull(@org.jetbrains.annotations.Nullable()
    T $this$notNull, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, kotlin.Unit> f) {
    }
    
    public static final <T extends java.lang.Object, R extends java.lang.Object>R ifNotNullOrElse(@org.jetbrains.annotations.Nullable()
    T $this$ifNotNullOrElse, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, ? extends R> ifNotNullPath, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends R> elsePath) {
        return null;
    }
}