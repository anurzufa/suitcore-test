package com.suitcore.firebase.remoteconfig;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H&J\u001c\u0010\b\u001a\u00020\u00032\b\u0010\t\u001a\u0004\u0018\u00010\u00072\b\u0010\n\u001a\u0004\u0018\u00010\u0007H&\u00a8\u0006\u000b"}, d2 = {"Lcom/suitcore/firebase/remoteconfig/RemoteConfigView;", "Lcom/suitcore/base/presenter/MvpView;", "onUpdateAppNeeded", "", "forceUpdate", "", "message", "", "onUpdateBaseUrlNeeded", "type", "url", "app_debug"})
public abstract interface RemoteConfigView extends com.suitcore.base.presenter.MvpView {
    
    public abstract void onUpdateAppNeeded(boolean forceUpdate, @org.jetbrains.annotations.Nullable()
    java.lang.String message);
    
    public abstract void onUpdateBaseUrlNeeded(@org.jetbrains.annotations.Nullable()
    java.lang.String type, @org.jetbrains.annotations.Nullable()
    java.lang.String url);
}