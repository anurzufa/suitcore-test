package com.suitcore.feature.event;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH&J\b\u0010\n\u001a\u00020\u0003H&J\u0018\u0010\u000b\u001a\u00020\u00032\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH&\u00a8\u0006\f"}, d2 = {"Lcom/suitcore/feature/event/EventView;", "Lcom/suitcore/base/presenter/MvpView;", "onFailed", "", "error", "", "onMemberCacheLoaded", "event", "", "Lcom/suitcore/data/model/Event;", "onMemberEmpty", "onMemberLoaded", "app_debug"})
public abstract interface EventView extends com.suitcore.base.presenter.MvpView {
    
    public abstract void onMemberCacheLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Event> event);
    
    public abstract void onMemberLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Event> event);
    
    public abstract void onMemberEmpty();
    
    public abstract void onFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error);
}