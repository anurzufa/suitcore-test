package com.suitcore.feature.guest;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u0012\u0010\u0011\u001a\u00020\u00102\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0010H\u0014J\u0012\u0010\u0015\u001a\u00020\u00102\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u00102\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0016J\b\u0010\u001c\u001a\u00020\u0010H\u0016J\u0018\u0010\u001d\u001a\u00020\u00102\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0016J\u0012\u0010\u001e\u001a\u00020\u00102\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0018\u0010!\u001a\u00020\u00102\u000e\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0002J\b\u0010#\u001a\u00020\u0010H\u0002J\b\u0010$\u001a\u00020\u0010H\u0002J\b\u0010%\u001a\u00020\u0010H\u0002J\b\u0010&\u001a\u00020\u0010H\u0002J\b\u0010\'\u001a\u00020\u0010H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\fX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006("}, d2 = {"Lcom/suitcore/feature/guest/GuestActivity;", "Lcom/suitcore/base/ui/BaseActivity;", "Lcom/suitcore/feature/guest/GuestView;", "Lcom/suitcore/feature/guest/GuestItemView$OnActionListener;", "()V", "guestAdapter", "Lcom/suitcore/feature/guest/GuestAdapter;", "guestPresenter", "Lcom/suitcore/feature/guest/GuestPresenter;", "layoutManager", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "resourceLayout", "", "getResourceLayout", "()I", "loadData", "", "onClicked", "view", "Lcom/suitcore/feature/guest/GuestItemView;", "onDestroy", "onFailed", "error", "", "onMemberCacheLoaded", "guests", "", "Lcom/suitcore/data/model/Guest;", "onMemberEmpty", "onMemberLoaded", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setData", "data", "setupEmptyView", "setupErrorView", "setupList", "setupPresenter", "setupProgressView", "app_debug"})
public final class GuestActivity extends com.suitcore.base.ui.BaseActivity implements com.suitcore.feature.guest.GuestView, com.suitcore.feature.guest.GuestItemView.OnActionListener {
    private com.suitcore.feature.guest.GuestPresenter guestPresenter;
    private com.suitcore.feature.guest.GuestAdapter guestAdapter;
    private androidx.recyclerview.widget.RecyclerView.LayoutManager layoutManager;
    private final int resourceLayout = com.suitcore.R.layout.activity_guest;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void setupPresenter() {
    }
    
    private final void setupList() {
    }
    
    private final void loadData() {
    }
    
    private final void setData(java.util.List<? extends com.suitcore.data.model.Guest> data) {
    }
    
    private final void setupProgressView() {
    }
    
    private final void setupEmptyView() {
    }
    
    private final void setupErrorView() {
    }
    
    @java.lang.Override()
    public void onMemberCacheLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Guest> guests) {
    }
    
    @java.lang.Override()
    public void onMemberLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.suitcore.data.model.Guest> guests) {
    }
    
    @java.lang.Override()
    public void onMemberEmpty() {
    }
    
    @java.lang.Override()
    public void onFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error) {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.Nullable()
    com.suitcore.feature.guest.GuestItemView view) {
    }
    
    public GuestActivity() {
        super();
    }
}