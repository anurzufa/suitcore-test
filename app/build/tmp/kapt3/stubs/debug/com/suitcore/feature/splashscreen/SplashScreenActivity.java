package com.suitcore.feature.splashscreen;

import java.lang.System;

/**
 * Created by dodydmw19 on 12/19/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\r\u001a\u00020\u0006H\u0002J\b\u0010\u000e\u001a\u00020\u0006H\u0002J\b\u0010\u000f\u001a\u00020\u0006H\u0016J\b\u0010\u0010\u001a\u00020\u0006H\u0016J\u0012\u0010\u0011\u001a\u00020\u00062\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0014J\b\u0010\u0014\u001a\u00020\u0006H\u0002R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\bX\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/suitcore/feature/splashscreen/SplashScreenActivity;", "Lcom/suitcore/base/ui/BaseActivity;", "Lcom/suitcore/feature/splashscreen/SplashScreenView;", "()V", "actionClicked", "Lkotlin/reflect/KFunction0;", "", "resourceLayout", "", "getResourceLayout", "()I", "splashScreenPresenter", "Lcom/suitcore/feature/splashscreen/SplashScreenPresenter;", "dialogPositiveAction", "handleIntent", "isLoginFalse", "navigateToMainView", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setupPresenter", "app_debug"})
public final class SplashScreenActivity extends com.suitcore.base.ui.BaseActivity implements com.suitcore.feature.splashscreen.SplashScreenView {
    private com.suitcore.feature.splashscreen.SplashScreenPresenter splashScreenPresenter;
    private final kotlin.reflect.KFunction<kotlin.Unit> actionClicked = null;
    private final int resourceLayout = com.suitcore.R.layout.activity_splashscreen;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void handleIntent() {
    }
    
    private final void setupPresenter() {
    }
    
    @java.lang.Override()
    public void navigateToMainView() {
    }
    
    @java.lang.Override()
    public void isLoginFalse() {
    }
    
    private final void dialogPositiveAction() {
    }
    
    public SplashScreenActivity() {
        super();
    }
}