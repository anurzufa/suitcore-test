package com.suitcore.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004\u00a8\u0006\u0006"}, d2 = {"Lcom/suitcore/data/model/EventDataDummy;", "", "()V", "mEventList", "", "Lcom/suitcore/data/model/Event;", "app_debug"})
public final class EventDataDummy {
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.suitcore.data.model.Event> mEventList() {
        return null;
    }
    
    public EventDataDummy() {
        super();
    }
}