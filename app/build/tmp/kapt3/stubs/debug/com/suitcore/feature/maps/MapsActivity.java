package com.suitcore.feature.maps;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u000b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0012\u0010\u001b\u001a\u00020\u00182\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u0018H\u0002J\u0010\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!H\u0016J\u0012\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u0010\'\u001a\u00020\u0018H\u0014J\u0018\u0010(\u001a\u00020\u00182\u000e\u0010)\u001a\n\u0012\u0004\u0012\u00020!\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020\u0018H\u0016J\u0010\u0010,\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\tH\u0016J\u0010\u0010-\u001a\u00020$2\u0006\u0010.\u001a\u00020/H\u0016J\b\u00100\u001a\u00020\u0018H\u0014J\u0010\u00101\u001a\u00020\u00182\u0006\u00102\u001a\u00020$H\u0016J-\u00103\u001a\u00020\u00182\u0006\u00104\u001a\u00020\u00142\u000e\u00105\u001a\n\u0012\u0006\b\u0001\u0012\u00020!062\u0006\u00107\u001a\u000208H\u0016\u00a2\u0006\u0002\u00109J\b\u0010:\u001a\u00020\u0018H\u0014J\u0010\u0010;\u001a\u00020\u00182\u0006\u0010<\u001a\u00020\u001dH\u0014J\b\u0010=\u001a\u00020\u0018H\u0014J\b\u0010>\u001a\u00020\u0018H\u0014J\u0012\u0010?\u001a\u00020\u00182\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0014J\b\u0010@\u001a\u00020\u0018H\u0002J\u0010\u0010A\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\tH\u0002J\b\u0010B\u001a\u00020\u0018H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u0014X\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006C"}, d2 = {"Lcom/suitcore/feature/maps/MapsActivity;", "Lcom/suitcore/base/ui/BaseActivity;", "Lcom/suitcore/feature/maps/MapsView;", "Lcom/mapbox/mapboxsdk/maps/OnMapReadyCallback;", "Lcom/mapbox/android/core/permissions/PermissionsListener;", "()V", "currentRoute", "Lcom/mapbox/api/directions/v5/models/DirectionsRoute;", "mapboxMap", "Lcom/mapbox/mapboxsdk/maps/MapboxMap;", "mapsPresenter", "Lcom/suitcore/feature/maps/MapsPresenter;", "markers", "Ljava/util/ArrayList;", "Lcom/mapbox/mapboxsdk/annotations/Marker;", "navigationMapRoute", "Lcom/mapbox/services/android/navigation/ui/v5/route/NavigationMapRoute;", "permissionsManager", "Lcom/mapbox/android/core/permissions/PermissionsManager;", "resourceLayout", "", "getResourceLayout", "()I", "enableLocationComponent", "", "loadedMapStyle", "Lcom/mapbox/mapboxsdk/maps/Style;", "initMapView", "savedInstanceState", "Landroid/os/Bundle;", "initPermissions", "isError", "msg", "", "isSuccess", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onExplanationNeeded", "permissionsToExplain", "", "onLowMemory", "onMapReady", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onPause", "onPermissionResult", "granted", "onRequestPermissionsResult", "requestCode", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onSaveInstanceState", "outState", "onStart", "onStop", "onViewReady", "setupPresenter", "showingDeviceLocation", "syncMapbox", "app_debug"})
public final class MapsActivity extends com.suitcore.base.ui.BaseActivity implements com.suitcore.feature.maps.MapsView, com.mapbox.mapboxsdk.maps.OnMapReadyCallback, com.mapbox.android.core.permissions.PermissionsListener {
    private com.suitcore.feature.maps.MapsPresenter mapsPresenter;
    private com.mapbox.android.core.permissions.PermissionsManager permissionsManager;
    private com.mapbox.mapboxsdk.maps.MapboxMap mapboxMap;
    private final java.util.ArrayList<com.mapbox.mapboxsdk.annotations.Marker> markers = null;
    private com.mapbox.api.directions.v5.models.DirectionsRoute currentRoute;
    private com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute navigationMapRoute;
    private final int resourceLayout = com.suitcore.R.layout.activity_maps;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState) {
    }
    
    private final void initMapView(android.os.Bundle savedInstanceState) {
    }
    
    private final void setupPresenter() {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.NotNull()
    com.mapbox.mapboxsdk.maps.MapboxMap mapboxMap) {
    }
    
    private final void showingDeviceLocation(com.mapbox.mapboxsdk.maps.MapboxMap mapboxMap) {
    }
    
    private final void enableLocationComponent(com.mapbox.mapboxsdk.maps.Style loadedMapStyle) {
    }
    
    private final void initPermissions() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void syncMapbox() {
    }
    
    @java.lang.Override()
    public void onExplanationNeeded(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissionsToExplain) {
    }
    
    @java.lang.Override()
    public void onPermissionResult(boolean granted) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void isSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    public void isError(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    public void onLowMemory() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public MapsActivity() {
        super();
    }
}