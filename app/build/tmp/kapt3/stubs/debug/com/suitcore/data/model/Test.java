package com.suitcore.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/suitcore/data/model/Test;", "Lio/realm/RealmObject;", "()V", "nama", "", "getNama", "()Ljava/lang/String;", "setNama", "(Ljava/lang/String;)V", "app_debug"})
public class Test extends io.realm.RealmObject {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String nama;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNama() {
        return null;
    }
    
    public final void setNama(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public Test() {
        super();
    }
}