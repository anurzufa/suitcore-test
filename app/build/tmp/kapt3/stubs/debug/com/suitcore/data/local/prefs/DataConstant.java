package com.suitcore.data.local.prefs;

import java.lang.System;

/**
 * Created by dodydmw19 on 7/23/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/suitcore/data/local/prefs/DataConstant;", "", "()V", "BASE_URL", "", "BIRTHDAY", "EVENT", "GUEST", "LOGIN", "NAME", "PLAYER_ID", "PREF_NAME", "session_status", "app_debug"})
public final class DataConstant {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PLAYER_ID = "player_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL = "base_url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NAME = "name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EVENT = "nameEvent";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GUEST = "nameGuest";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String session_status = "session_status";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PREF_NAME = "LOGIN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN = "IS_LOGIN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BIRTHDAY = "birthdayGuest";
    public static final com.suitcore.data.local.prefs.DataConstant INSTANCE = null;
    
    private DataConstant() {
        super();
    }
}