package com.suitcore.firebase.remoteconfig;

import java.lang.System;

/**
 * Field Params :
 * force_message -> for message content force update
 * info_message -> for message content info update (can deny)
 * minumum_force_android -> latest versionCode for force update
 * minimum_info_android -> latest versionCode for info update
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016J\u000e\u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eJ\b\u0010\u000f\u001a\u00020\nH\u0002J\b\u0010\u0010\u001a\u00020\nH\u0016J\b\u0010\u0011\u001a\u00020\nH\u0016J\b\u0010\u0012\u001a\u00020\nH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/suitcore/firebase/remoteconfig/RemoteConfigPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/firebase/remoteconfig/RemoteConfigView;", "()V", "cacheExpiration", "", "mFireBaseRemoteConfig", "Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;", "mvpView", "attachView", "", "view", "checkUpdate", "type", "", "checkVersion", "detachView", "onDestroy", "setupFireBaseRemoteConfig", "app_debug"})
public final class RemoteConfigPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.firebase.remoteconfig.RemoteConfigView> {
    private com.google.firebase.remoteconfig.FirebaseRemoteConfig mFireBaseRemoteConfig;
    private com.suitcore.firebase.remoteconfig.RemoteConfigView mvpView;
    private final long cacheExpiration = 0L;
    
    private final void setupFireBaseRemoteConfig() {
    }
    
    public final void checkUpdate(@org.jetbrains.annotations.NotNull()
    java.lang.String type) {
    }
    
    private final void checkVersion() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.firebase.remoteconfig.RemoteConfigView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public RemoteConfigPresenter() {
        super();
    }
}