package com.suitcore.base.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b&\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0016\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aJ\b\u0010\u001b\u001a\u00020\u0017H\u0002J\u0010\u0010\u001c\u001a\u00020\u00172\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eJ\u0010\u0010\u001f\u001a\u00020\u00172\b\u0010 \u001a\u0004\u0018\u00010\u001eJ0\u0010!\u001a\u00020\u00172\u000e\u0010\"\u001a\n\u0012\u0006\b\u0001\u0012\u00020$0#2\b\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020(J(\u0010!\u001a\u00020\u00172\u0006\u0010*\u001a\u00020\u00112\u000e\u0010\"\u001a\n\u0012\u0006\b\u0001\u0012\u00020$0#2\b\u0010%\u001a\u0004\u0018\u00010&J\b\u0010+\u001a\u00020\u0017H\u0016J\b\u0010,\u001a\u00020\u0017H\u0016J\u0012\u0010-\u001a\u00020\u00172\b\u0010.\u001a\u0004\u0018\u00010&H\u0014J\u0010\u0010/\u001a\u00020(2\u0006\u00100\u001a\u000201H\u0016J\u0012\u00102\u001a\u00020\u00172\b\u0010.\u001a\u0004\u0018\u00010&H$J\u0010\u00103\u001a\u00020\u00172\u0006\u00104\u001a\u00020\u0011H\u0016J\"\u00105\u001a\u00020\u00172\u0006\u00106\u001a\u00020\u00152\u0006\u00107\u001a\u00020(2\b\u00108\u001a\u0004\u0018\u000109H\u0003J \u00105\u001a\u00020\u00172\u0006\u00104\u001a\u00020:2\u0006\u00106\u001a\u00020\u00152\u0006\u00107\u001a\u00020(H\u0004J\u0010\u0010;\u001a\u00020\u00172\u0006\u0010<\u001a\u00020\u0011H\u0016J\u0010\u0010;\u001a\u00020\u00172\u0006\u0010<\u001a\u00020:H\u0016J\u001e\u0010=\u001a\u00020\u00172\u0006\u0010<\u001a\u00020\u00112\f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00170?H\u0016J\u001e\u0010=\u001a\u00020\u00172\u0006\u0010<\u001a\u00020:2\f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00170?H\u0016J\u001e\u0010@\u001a\u00020\u00172\u0006\u0010<\u001a\u00020:2\f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00170?H\u0016J)\u0010A\u001a\u00020\u00172\u0006\u0010B\u001a\u00020(2\b\u0010<\u001a\u0004\u0018\u00010:2\b\u0010C\u001a\u0004\u0018\u00010\u0011H\u0016\u00a2\u0006\u0002\u0010DJ\u0010\u0010E\u001a\u00020\u00172\u0006\u0010F\u001a\u00020\u0011H\u0016J\u0010\u0010E\u001a\u00020\u00172\u0006\u0010F\u001a\u00020:H\u0016J\u0010\u0010G\u001a\u00020\u00172\u0006\u0010<\u001a\u00020:H\u0004R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00020\u0011X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"}, d2 = {"Lcom/suitcore/base/ui/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/suitcore/base/presenter/MvpView;", "()V", "activityIntent", "Landroid/content/Intent;", "baseFragmentManager", "Landroidx/fragment/app/FragmentManager;", "getBaseFragmentManager", "()Landroidx/fragment/app/FragmentManager;", "mActionBar", "Landroidx/appcompat/app/ActionBar;", "mCommonLoadingDialog", "Lcom/suitcore/helper/CommonLoadingDialog;", "mInflater", "Landroid/view/LayoutInflater;", "resourceLayout", "", "getResourceLayout", "()I", "toolBar", "Landroidx/appcompat/widget/Toolbar;", "changeProgressBarColor", "", "color", "progressBar", "Landroid/widget/ProgressBar;", "clearActivity", "clearRecyclerView", "recyclerView", "Lcom/suitcore/base/ui/recyclerview/BaseRecyclerView;", "finishLoad", "recycler", "goToActivity", "actDestination", "Ljava/lang/Class;", "Landroid/app/Activity;", "data", "Landroid/os/Bundle;", "clearIntent", "", "isFinish", "resultCode", "hideLoading", "onBackPressed", "onCreate", "savedInstanceState", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onViewReady", "setTitle", "title", "setupToolbar", "baseToolbar", "needHomeButton", "onClickListener", "Landroid/view/View$OnClickListener;", "", "showAlertDialog", "message", "showConfirmationDialog", "confirmCallback", "Lkotlin/Function0;", "showConfirmationSingleDialog", "showLoading", "isBackPressedCancelable", "currentPage", "(ZLjava/lang/String;Ljava/lang/Integer;)V", "showLoadingWithText", "msg", "showToast", "app_debug"})
public abstract class BaseActivity extends androidx.appcompat.app.AppCompatActivity implements com.suitcore.base.presenter.MvpView {
    private androidx.appcompat.widget.Toolbar toolBar;
    private android.view.LayoutInflater mInflater;
    private androidx.appcompat.app.ActionBar mActionBar;
    private com.suitcore.helper.CommonLoadingDialog mCommonLoadingDialog;
    private android.content.Intent activityIntent;
    private java.util.HashMap _$_findViewCache;
    
    private final androidx.fragment.app.FragmentManager getBaseFragmentManager() {
        return null;
    }
    
    protected abstract int getResourceLayout();
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    protected final void setupToolbar(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    androidx.appcompat.widget.Toolbar baseToolbar, boolean needHomeButton) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.LOLLIPOP)
    private final void setupToolbar(androidx.appcompat.widget.Toolbar baseToolbar, boolean needHomeButton, android.view.View.OnClickListener onClickListener) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void setTitle(int title) {
    }
    
    public final void changeProgressBarColor(int color, @org.jetbrains.annotations.NotNull()
    android.widget.ProgressBar progressBar) {
    }
    
    private final void clearActivity() {
    }
    
    public final void goToActivity(@org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends android.app.Activity> actDestination, @org.jetbrains.annotations.Nullable()
    android.os.Bundle data, boolean clearIntent, boolean isFinish) {
    }
    
    public final void goToActivity(int resultCode, @org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends android.app.Activity> actDestination, @org.jetbrains.annotations.Nullable()
    android.os.Bundle data) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    protected final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    protected abstract void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState);
    
    @java.lang.Override()
    public void showLoading(boolean isBackPressedCancelable, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Integer currentPage) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(int msg) {
    }
    
    @java.lang.Override()
    public void hideLoading() {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationSingleDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(int message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(int message) {
    }
    
    public final void finishLoad(@org.jetbrains.annotations.Nullable()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView recycler) {
    }
    
    public final void clearRecyclerView(@org.jetbrains.annotations.Nullable()
    com.suitcore.base.ui.recyclerview.BaseRecyclerView recyclerView) {
    }
    
    public BaseActivity() {
        super();
    }
}