package com.suitcore.helper;

import java.lang.System;

/**
 * Created by DODYDMW19 on 6/6/2016.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u0000 \b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\bB\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007\u00a8\u0006\t"}, d2 = {"Lcom/suitcore/helper/AppStatus;", "", "(Ljava/lang/String;I)V", "NONE", "WIFI", "MOBILE", "OTHER", "MAYBE", "Companion", "app_debug"})
public enum AppStatus {
    /*public static final*/ NONE /* = new NONE() */,
    /*public static final*/ WIFI /* = new WIFI() */,
    /*public static final*/ MOBILE /* = new MOBILE() */,
    /*public static final*/ OTHER /* = new OTHER() */,
    /*public static final*/ MAYBE /* = new MAYBE() */;
    public static final com.suitcore.helper.AppStatus.Companion Companion = null;
    
    AppStatus() {
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/suitcore/helper/AppStatus$Companion;", "", "()V", "checkConnectivity", "Lcom/suitcore/helper/AppStatus;", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.suitcore.helper.AppStatus checkConnectivity(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}