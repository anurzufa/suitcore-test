package com.suitcore.feature.guest;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0016J\u0006\u0010\f\u001a\u00020\u0002J\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\bR\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/suitcore/feature/guest/GuestItemView;", "Lcom/suitcore/base/ui/adapter/viewholder/BaseItemViewHolder;", "Lcom/suitcore/data/model/Guest;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "guest", "mActionListener", "Lcom/suitcore/feature/guest/GuestItemView$OnActionListener;", "bind", "", "data", "getData", "setOnActionListener", "listener", "OnActionListener", "app_debug"})
public final class GuestItemView extends com.suitcore.base.ui.adapter.viewholder.BaseItemViewHolder<com.suitcore.data.model.Guest> {
    private com.suitcore.feature.guest.GuestItemView.OnActionListener mActionListener;
    private com.suitcore.data.model.Guest guest;
    
    @java.lang.Override()
    public void bind(@org.jetbrains.annotations.Nullable()
    com.suitcore.data.model.Guest data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.suitcore.data.model.Guest getData() {
        return null;
    }
    
    public final void setOnActionListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.guest.GuestItemView.OnActionListener listener) {
    }
    
    public GuestItemView(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/suitcore/feature/guest/GuestItemView$OnActionListener;", "", "onClicked", "", "view", "Lcom/suitcore/feature/guest/GuestItemView;", "app_debug"})
    public static abstract interface OnActionListener {
        
        public abstract void onClicked(@org.jetbrains.annotations.Nullable()
        com.suitcore.feature.guest.GuestItemView view);
    }
}