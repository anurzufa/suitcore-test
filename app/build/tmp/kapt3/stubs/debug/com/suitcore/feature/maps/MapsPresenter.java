package com.suitcore.feature.maps;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\b\u0010\b\u001a\u00020\u0006H\u0016J\b\u0010\t\u001a\u00020\u0006H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/suitcore/feature/maps/MapsPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/feature/maps/MapsView;", "()V", "mapsView", "attachView", "", "view", "detachView", "onDestroy", "app_debug"})
public final class MapsPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.feature.maps.MapsView> {
    private com.suitcore.feature.maps.MapsView mapsView;
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.maps.MapsView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public MapsPresenter() {
        super();
    }
}