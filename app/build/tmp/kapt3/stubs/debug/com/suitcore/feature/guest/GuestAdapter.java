package com.suitcore.feature.guest;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0014J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\bH\u0016J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0006R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/suitcore/feature/guest/GuestAdapter;", "Lcom/suitcore/base/ui/adapter/BaseRecyclerAdapter;", "Lcom/suitcore/data/model/Guest;", "Lcom/suitcore/feature/guest/GuestItemView;", "()V", "mOnActionListener", "Lcom/suitcore/feature/guest/GuestItemView$OnActionListener;", "getItemResourceLayout", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setOnActionListener", "", "onActionListener", "app_debug"})
public final class GuestAdapter extends com.suitcore.base.ui.adapter.BaseRecyclerAdapter<com.suitcore.data.model.Guest, com.suitcore.feature.guest.GuestItemView> {
    private com.suitcore.feature.guest.GuestItemView.OnActionListener mOnActionListener;
    
    public final void setOnActionListener(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.guest.GuestItemView.OnActionListener onActionListener) {
    }
    
    @java.lang.Override()
    protected int getItemResourceLayout() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.suitcore.feature.guest.GuestItemView onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    public GuestAdapter() {
        super();
    }
}