package com.suitcore.feature.main;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\b\u0010\u000b\u001a\u00020\tH\u0016J\u0006\u0010\f\u001a\u00020\tJ\b\u0010\r\u001a\u00020\tH\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/suitcore/feature/main/MainPresenter;", "Lcom/suitcore/base/presenter/BasePresenter;", "Lcom/suitcore/feature/main/MainView;", "()V", "mvpView", "realmHelperUser", "Lcom/suitcore/data/local/RealmHelper;", "Lcom/suitcore/data/model/User;", "attachView", "", "view", "detachView", "getUser", "onDestroy", "app_debug"})
public final class MainPresenter implements com.suitcore.base.presenter.BasePresenter<com.suitcore.feature.main.MainView> {
    private com.suitcore.data.local.RealmHelper<com.suitcore.data.model.User> realmHelperUser;
    private com.suitcore.feature.main.MainView mvpView;
    
    public final void getUser() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.suitcore.feature.main.MainView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public MainPresenter() {
        super();
    }
}