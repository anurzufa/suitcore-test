package com.suitcore.data.local.prefs;

import java.lang.System;

/**
 * Created by dodydmw19 on 7/18/18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0010\b\u0016\u0018\u0000 )2\u00020\u0001:\u0001)B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\n\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\u0010J\u0015\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\u0013J)\u0010\u0014\u001a\u0004\u0018\u0001H\u0015\"\u0004\b\u0000\u0010\u00152\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u00150\u0017\u00a2\u0006\u0002\u0010\u0018J*\u0010\u0019\u001a\n\u0012\u0004\u0012\u0002H\u0015\u0018\u00010\u001a\"\u0004\b\u0000\u0010\u00152\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u00150\u0017J\u0010\u0010\u001b\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u001c\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0016\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u0006J\u0016\u0010 \u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\fJ\u0016\u0010!\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u000fJ\u0016\u0010\"\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u0012J!\u0010#\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u00152\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010$\u001a\u0002H\u0015\u00a2\u0006\u0002\u0010%J\"\u0010&\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u00152\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\'\u001a\b\u0012\u0004\u0012\u0002H\u00150\u001aJ\u0016\u0010(\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\b\u00a8\u0006*"}, d2 = {"Lcom/suitcore/data/local/prefs/SuitPreferences;", "", "()V", "clearSession", "", "deleteValue", "", "key", "", "(Ljava/lang/String;)Ljava/lang/Boolean;", "getBoolean", "getFloat", "", "(Ljava/lang/String;)Ljava/lang/Float;", "getInt", "", "(Ljava/lang/String;)Ljava/lang/Integer;", "getLong", "", "(Ljava/lang/String;)Ljava/lang/Long;", "getObject", "T", "classType", "Ljava/lang/Class;", "(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;", "getObjectsList", "", "getString", "getStringWithDefaultUrl", "isKeyExists", "saveBoolean", "value", "saveFloat", "saveInt", "saveLong", "saveObject", "object", "(Ljava/lang/String;Ljava/lang/Object;)V", "saveObjectsList", "objectList", "saveString", "Companion", "app_debug"})
public class SuitPreferences {
    private static com.suitcore.data.local.prefs.SuitPreferences instance;
    private static android.content.SharedPreferences mSharedPreferences;
    public static final com.suitcore.data.local.prefs.SuitPreferences.Companion Companion = null;
    
    public final void saveInt(@org.jetbrains.annotations.NotNull()
    java.lang.String key, int value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getInt(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void saveBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key, boolean value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void saveFloat(@org.jetbrains.annotations.NotNull()
    java.lang.String key, float value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getFloat(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void saveLong(@org.jetbrains.annotations.NotNull()
    java.lang.String key, long value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getLong(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void saveString(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getString(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStringWithDefaultUrl(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final <T extends java.lang.Object>void saveObject(@org.jetbrains.annotations.NotNull()
    java.lang.String key, T object) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final <T extends java.lang.Object>T getObject(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Class<T> classType) {
        return null;
    }
    
    public final <T extends java.lang.Object>void saveObjectsList(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends T> objectList) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final <T extends java.lang.Object>java.util.List<T> getObjectsList(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.Class<T> classType) {
        return null;
    }
    
    public final void clearSession() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean deleteValue(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    private final boolean isKeyExists(java.lang.String key) {
        return false;
    }
    
    public SuitPreferences() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000b\u001a\u00020\bH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/suitcore/data/local/prefs/SuitPreferences$Companion;", "", "()V", "instance", "Lcom/suitcore/data/local/prefs/SuitPreferences;", "mSharedPreferences", "Landroid/content/SharedPreferences;", "init", "", "context", "Landroid/content/Context;", "validateInitialization", "app_debug"})
    public static final class Companion {
        
        public final void init(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.suitcore.data.local.prefs.SuitPreferences instance() {
            return null;
        }
        
        private final void validateInitialization() {
        }
        
        private Companion() {
            super();
        }
    }
}