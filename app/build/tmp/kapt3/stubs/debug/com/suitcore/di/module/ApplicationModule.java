package com.suitcore.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\b\u0007J\r\u0010\b\u001a\u00020\u0003H\u0001\u00a2\u0006\u0002\b\tJ\r\u0010\n\u001a\u00020\u000bH\u0001\u00a2\u0006\u0002\b\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/suitcore/di/module/ApplicationModule;", "", "mApplication", "Landroid/app/Application;", "(Landroid/app/Application;)V", "provideAPIService", "Lcom/suitcore/data/remote/services/APIService;", "provideAPIService$app_debug", "provideApplication", "provideApplication$app_debug", "provideContext", "Landroid/content/Context;", "provideContext$app_debug", "app_debug"})
@dagger.Module()
public final class ApplicationModule {
    private final android.app.Application mApplication = null;
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final android.app.Application provideApplication$app_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @ApplicationContext()
    @dagger.Provides()
    public final android.content.Context provideContext$app_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @com.suitcore.di.scope.SuitCoreApplicationScope()
    @dagger.Provides()
    public final com.suitcore.data.remote.services.APIService provideAPIService$app_debug() {
        return null;
    }
    
    public ApplicationModule(@org.jetbrains.annotations.NotNull()
    android.app.Application mApplication) {
        super();
    }
}