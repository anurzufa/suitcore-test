package com.suitcore.di.component

import com.suitcore.di.module.ApplicationModule
import com.suitcore.di.scope.SuitCoreApplicationScope
import com.suitcore.feature.event.EventPresenter
import com.suitcore.feature.guest.GuestPresenter
import com.suitcore.feature.login.LoginPresenter
import com.suitcore.feature.main.MainPresenter
import com.suitcore.feature.maps.MapsPresenter
import com.suitcore.feature.splashscreen.SplashScreenPresenter
import com.suitcore.firebase.remoteconfig.RemoteConfigPresenter
import com.suitcore.onesignal.OneSignalPresenter
import dagger.Component

@SuitCoreApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(splashScreenPresenter: SplashScreenPresenter)

    fun inject(oneSignalPresenter: OneSignalPresenter)

    fun inject(remoteConfigPresenter: RemoteConfigPresenter)

    fun inject(eventPresenter: EventPresenter)

    fun inject(guestPresenter: GuestPresenter)

    fun inject(mainPresenter: MainPresenter)

    fun inject(loginPresenter: LoginPresenter)

    fun inject(mapsPresenter: MapsPresenter)
}