package com.suitcore.data.local

import io.realm.Realm
import io.realm.RealmObject
import io.realm.Sort

/**
 * Created by dodydmw19 on 7/25/18.
 */

class RealmHelper<T : RealmObject> {

    fun saveObject(data: T) {
        var realm: Realm? = null
        try {
            realm = Realm.getDefaultInstance()
            realm?.executeTransaction { r -> r.copyToRealmOrUpdate(data) }
        } finally {
            realm?.close()
        }
    }

    fun saveData(data: T) {
        val realm: Realm? = null
        realm?.executeTransaction { realm ->
            realm.copyToRealmOrUpdate(data)
        }
    }

    fun saveList(data: List<T>?) {
        var realm: Realm? = null
        try {
            realm = Realm.getDefaultInstance()
            realm?.executeTransaction { r -> r.copyToRealmOrUpdate(data) }
        } finally {
            realm?.close()
        }
    }

    fun getData(id: Int, paramName: String, data: T): T? {
        val realm: Realm = Realm.getDefaultInstance()
        val cache = realm.where(data::class.java).equalTo(paramName, id).findFirst()!!
        var validData: T? = null
        if (cache.isValid) {
            validData = realm.copyFromRealm(cache)
        }
        return validData
    }

    fun getData(data: T): List<T>? {
        val realm: Realm = Realm.getDefaultInstance()
        val cache = realm.where(data::class.java).findAll()!!
        var validData: List<T>? = emptyList()
        if (cache.isValid) {
            validData = realm.copyFromRealm(cache)
        }
        return validData
    }

    fun getDataSorted(data: T): List<T>? {
        val realm: Realm = Realm.getDefaultInstance()
        val cache = realm.where(data::class.java).sort("date", Sort.DESCENDING).findAll()!!
        var validData: List<T>? = emptyList()
        if (cache.isValid) {
            validData = realm.copyFromRealm(cache)
        }
        return validData
    }

    fun getSingleData(data: T): T? {
        val realm: Realm = Realm.getDefaultInstance()
        val cache = realm.where(data::class.java).findFirst()!!
        var validData: T? = null
        if (cache.isValid) {
            validData = realm.copyFromRealm(cache)
        }
        return validData
    }

    fun getDataListQuery(id: Int, paramName: String, data: T): List<T>? {
        val realm: Realm = Realm.getDefaultInstance()
        val cache = realm.where(data::class.java).equalTo(paramName, id).findAll()!!
        var validData: List<T>? = null
        if (cache.isValid) {
            validData = realm.copyFromRealm(cache)
        }
        return validData
    }

    fun deleteData(data: T) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.where(data::class.java).findAll().deleteAllFromRealm()
        realm.commitTransaction()
        realm.close()
    }

    fun removeAllData() {
        val realm: Realm = Realm.getDefaultInstance()
        realm.use { r ->
            r.executeTransaction { rr ->
                rr.deleteAll()
            }
        }
    }

}