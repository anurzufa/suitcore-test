package com.suitcore.data.local.prefs

/**
 * Created by dodydmw19 on 7/23/18.
 */

object DataConstant {

    // for Preference Data Constant
    const val PLAYER_ID = "player_id"
    const val BASE_URL = "base_url"

    const val NAME = "name"//for Preference UserName
    const val EVENT = "nameEvent"//for Preference EventName6
    const val GUEST = "nameGuest"//for Preference GuestName
    const val session_status = "session_status"

    const val PREF_NAME = "LOGIN"
    const val LOGIN = "IS_LOGIN"

    const val BIRTHDAY = "birthdayGuest"


}