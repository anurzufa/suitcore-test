package com.suitcore.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Locations(
        @PrimaryKey
        var id : Int? = 0,
        var name: String? = null,
        var detail: String? = null,
        var latitude: Double? = null,
        var longitude: Double? = null,
        var image: Int? = 0
) : RealmObject()