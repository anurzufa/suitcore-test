package com.suitcore.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Event(
        @PrimaryKey
        var id: Int? = null,
        var nama: String? = null,
        var tanggal: String? = null,
        var image: Int? = 0) : RealmObject()