package com.suitcore.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class User : RealmObject(){
        @PrimaryKey
        var id: Int? = 0
        var nama: String? = null
}
