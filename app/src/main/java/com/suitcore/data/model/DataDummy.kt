package com.suitcore.data.model

import com.suitcore.R
import java.util.*

class EventDataDummy {

    fun mEventList(): List<Event>? {
        val konserRockBand = Event(1,"Konser Rock Band ", "20 November", R.drawable.concert)
        val liverMusicBatu = Event(2,"Liver Music Batu", "15 Desember", R.drawable.livemusic)
        val koktailSosialisasi = Event(3,"Koktail Sosialisasi", "24 Januari", R.drawable.cocktails)
        val gelanggangPekanRaya = Event(4,"Gelanggang Pekan Raya", "27 Februari", R.drawable.fairground)
        val konserKinerjaMusik = Event(5,"Konser Kinerja Musik", "18 Maret", R.drawable.concert2)
        val eventList: MutableList<Event> = ArrayList()
        eventList.add(konserRockBand)
        eventList.add(liverMusicBatu)
        eventList.add(koktailSosialisasi)
        eventList.add(gelanggangPekanRaya)
        eventList.add(konserKinerjaMusik)
        return eventList
    }

}