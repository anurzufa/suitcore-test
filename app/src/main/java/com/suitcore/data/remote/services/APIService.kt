package com.suitcore.data.remote.services

import com.suitcore.data.model.Guest
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by DODYDMW19 on 8/3/2017.
 */

interface APIService {

    @GET("596dec7f0f000023032b8017")
    fun getGuest(): Flowable<List<Guest>>

}
