package com.suitcore.feature.maps

import com.suitcore.base.presenter.MvpView
import com.suitcore.data.model.Locations

interface MapsView : MvpView{

    fun isSuccess(msg: String)
    fun isError(msg: String)
    fun onMemberCacheLoaded(locations : List<Locations>?)

    fun onMemberLoaded(locations: List<Locations>?)

    fun onMemberEmpty()

    fun onFailed(error: Any?)

}