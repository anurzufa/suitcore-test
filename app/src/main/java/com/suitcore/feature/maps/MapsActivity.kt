package com.suitcore.feature.maps

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.base.ui.recyclerview.BaseRecyclerView
import com.suitcore.data.model.ErrorCodeHelper
import com.suitcore.data.model.Locations
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapsActivity : BaseActivity(), MapsView, OnMapReadyCallback, PermissionsListener {

    private var mapsPresenter: MapsPresenter? = null
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap
    private lateinit var mapView: MapView
    private lateinit var featureCollection: FeatureCollection
    private val markers = ArrayList<Marker>()
    private lateinit var currentRoute: DirectionsRoute
    private var navigationMapRoute: NavigationMapRoute? = null
    private var mapsAdapter: MapsAdapter? = MapsAdapter()

    override val resourceLayout: Int = R.layout.activity_maps

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar("MapsActivity", toolbarMaps, true)
        map_view.getMapAsync(this)
        initMapView(savedInstanceState)
        initPermissions()
        setupPresenter()
        setupProgressView()
        setupList()
        setupEmptyView()
        setupErrorView()
        btnStartNavigation.setOnClickListener {
            val navigationLauncherOption = NavigationLauncherOptions.builder()
                    .directionsRoute(currentRoute)
                    .shouldSimulateRoute(true)
                    .build()
            NavigationLauncher.startNavigation(this, navigationLauncherOption)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        map_view.onSaveInstanceState(outState)
    }

    private fun initMapView(savedInstanceState: Bundle?) {
        map_view.onCreate(savedInstanceState)
    }


    override fun onMapReady(mapboxMap: MapboxMap) {

        this.mapboxMap = mapboxMap
        this.mapboxMap.setStyle(Style.MAPBOX_STREETS) {
            showingDeviceLocation(mapboxMap)
            enableLocationComponent(it)
        }
        this.mapboxMap.addOnMapClickListener {
            if (markers.size == 2) {
                mapboxMap.removeMarker(markers[1])
                markers.removeAt(1)
            }
            markers.add(
                    mapboxMap.addMarker(
                            MarkerOptions().position(it)
                    )
            )
            if (markers.size == 2) {
                val originPoint =
                        Point.fromLngLat(markers[0].position.longitude, markers[0].position.latitude)
                val destinationPoint =
                        Point.fromLngLat(markers[1].position.longitude, markers[1].position.latitude)
                NavigationRoute.builder(this)
                        .accessToken(Mapbox.getAccessToken()!!)
                        .origin(originPoint)
                        .destination(destinationPoint)
                        .voiceUnits(DirectionsCriteria.IMPERIAL)
                        .build()
                        .getRoute(object : Callback<DirectionsResponse> {
                            override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                                showToast("Error occured: ${t.message}")
                            }

                            override fun onResponse(
                                    call: Call<DirectionsResponse>,
                                    response: Response<DirectionsResponse>
                            ) {
                                if (response.body() == null) {
                                    showToast("No routes found, make sure you set the right user and access token.")
                                    btnStartNavigation.visibility = View.GONE
                                    return
                                } else if (response.body()!!.routes().size < 1) {
                                    showToast("No routes found")
                                    btnStartNavigation.visibility = View.GONE
                                    return
                                }
                                currentRoute = response.body()!!.routes()[0]
                                if (navigationMapRoute != null) {
                                    navigationMapRoute?.removeRoute()
                                } else {
                                    navigationMapRoute = NavigationMapRoute(
                                            null,
                                            map_view,
                                            mapboxMap,
                                            R.style.NavigationMapRoute
                                    )
                                }
                                navigationMapRoute?.addRoute(currentRoute)
                                btnStartNavigation.visibility = View.VISIBLE
                            }
                        })
            } else {
                btnStartNavigation.visibility = View.GONE
            }
            true
        }
        this.mapboxMap.setOnMarkerClickListener {
            for (marker in markers) {
                if (marker.position == it.position) {
                    markers.remove(marker)
                    mapboxMap.removeMarker(marker)
                    break
                }
            }
            true
        }
    }

    private fun showingDeviceLocation(mapboxMap: MapboxMap) {
        val locationComponent = mapboxMap.locationComponent
        locationComponent.activateLocationComponent(this, mapboxMap.style!!)
        locationComponent.isLocationComponentEnabled = true
        locationComponent.cameraMode = CameraMode.TRACKING
        locationComponent.renderMode = RenderMode.COMPASS

    }

    private fun enableLocationComponent(loadedMapStyle: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            val customLocationComponentOptions = LocationComponentOptions.builder(this)
                    .trackingGesturesManagement(true)
                    .accuracyColor(ContextCompat.getColor(this, R.color.mapbox_blue))
                    .build()

            val locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOptions)
                            .build()
            mapboxMap.locationComponent.apply {
                activateLocationComponent(locationComponentActivationOptions)
                isLocationComponentEnabled = true
                cameraMode = CameraMode.TRACKING
                renderMode = RenderMode.COMPASS
            }
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    private fun initPermissions() {
        val permissionListener = object : PermissionsListener {
            override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {}
            override fun onPermissionResult(granted: Boolean) {
                if (granted) {
                    syncMapbox()
                } else {
                    val alertDialogInfo = AlertDialog.Builder(this@MapsActivity)
                            .setTitle(getString(R.string.txt_info))
                            .setCancelable(false)
                            .setMessage(getString(R.string.txt_permissions_denied))
                            .setPositiveButton(getString(R.string.txt_dismiss)) { _, _ ->
                                finish()
                            }
                            .create()
                    alertDialogInfo.show()
                }
            }
        }
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            syncMapbox()
        } else {
            permissionsManager = PermissionsManager(permissionListener)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun syncMapbox() {
        map_view.getMapAsync(this)
    }


    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        showToast("Mohon acc permissionya ya kak")
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            showToast("Mohon acc permissionya ya kak")
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_maps, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_change_style -> {
                val items = arrayOf(
                        "Mapbox Street",
                        "Outdoor",
                        "Light",
                        "Dark",
                        "Satellite",
                        "Satellite Street",
                        "Traffic Day",
                        "Traffic Night"
                )
                val alertDialogChangeStyleMaps = AlertDialog.Builder(this)
                        .setItems(items) { dialog, item ->
                            when (item) {
                                0 -> {
                                    mapboxMap.setStyle(Style.MAPBOX_STREETS)
                                    dialog.dismiss()
                                }
                                1 -> {
                                    mapboxMap.setStyle(Style.OUTDOORS)
                                    dialog.dismiss()
                                }
                                2 -> {
                                    mapboxMap.setStyle(Style.LIGHT)
                                    dialog.dismiss()
                                }
                                3 -> {
                                    mapboxMap.setStyle(Style.DARK)
                                    dialog.dismiss()
                                }
                                4 -> {
                                    mapboxMap.setStyle(Style.SATELLITE)
                                    dialog.dismiss()
                                }
                                5 -> {
                                    mapboxMap.setStyle(Style.SATELLITE_STREETS)
                                    dialog.dismiss()
                                }
                                6 -> {
                                    mapboxMap.setStyle(Style.TRAFFIC_DAY)
                                    dialog.dismiss()
                                }
                                7 -> {
                                    mapboxMap.setStyle(Style.TRAFFIC_NIGHT)
                                    dialog.dismiss()
                                }
                            }
                        }
                        .setTitle(getString(R.string.txt_change_style_maps))
                        .create()
                alertDialogChangeStyleMaps.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun isSuccess(msg: String) {
        showToast(msg)
    }

    override fun isError(msg: String) {
        showToast(msg)
    }

    private fun setupPresenter() {
        mapsPresenter = MapsPresenter()
        mapsPresenter?.attachView(this)
        mapsPresenter?.getMapsCache()

        rvMaps.hideEmpty()
        rvMaps.showShimmer()
        mapsPresenter?.getEvent()
    }

    private fun setupList() {
        rvMaps.apply {
            setUpAsList()
            setAdapter(mapsAdapter)
            setPullToRefreshEnable(true)
            setLoadingMoreEnabled(false)
            setLoadingListener(object : XRecyclerView.LoadingListener {
                override fun onLoadMore() {
                    loadData()
                }

                override fun onRefresh() {
                    loadData()
                }
            })
        }
        mapsAdapter?.setActionListener(this)
        rvMaps.showShimmer()
    }


    private fun loadData() {
        mapsPresenter?.getEvent()
    }

    private fun setData(data: List<Locations>?) {
        data?.let {
            mapsAdapter?.add(it)
            rvMaps.stopShimmer()
            rvMaps.showRecycler()
        }
    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_guest.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        rvMaps.setImageEmptyView(R.drawable.empty_state)
        rvMaps.setTitleEmptyView(getString(R.string.txt_empty_member))
        rvMaps.setContentEmptyView(getString(R.string.txt_empty_member_content))
        rvMaps.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }
        })

    }

    private fun setupErrorView() {
        rvMaps.setImageErrorView(R.drawable.empty_state)
        rvMaps.setTitleErrorView(getString(R.string.txt_error_no_internet))
        rvMaps.setContentErrorView(getString(R.string.txt_error_connection))
        rvMaps.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })
    }

    override fun onMemberCacheLoaded(locations: List<Locations>?) {
        locations.let {
            if (locations?.isNotEmpty()!!) {
                setData(locations)
            }
        }
        finishLoad(rvMaps)
    }

    override fun onMemberLoaded(locations: List<Locations>?) {
        locations.let {
            if (locations?.isNotEmpty()!!) {
                setData(locations)
            }
        }
        finishLoad(rvMaps)
    }

    override fun onMemberEmpty() {
        finishLoad(rvMaps)
    }

    override fun onFailed(error: Any?) {
        finishLoad(rvMaps)
        rvMaps.showEmpty()
        error?.let { ErrorCodeHelper.getErrorMessage(this, it)?.let { msg -> showToast(msg) } }
    }

    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onStop() {
        super.onStop()
        map_view.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_view.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        map_view.onDestroy()
    }


}
