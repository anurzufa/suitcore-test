package com.suitcore.feature.maps

import android.view.View
import com.suitcore.base.ui.adapter.viewholder.BaseItemViewHolder
import com.suitcore.data.model.Locations
import kotlinx.android.synthetic.main.item_maps.view.*


class MapsItemView(itemView: View) : BaseItemViewHolder<Locations>(itemView) {

    private var mActionListener : onActionListener? = null
    private var mMaps : Locations? =null

    override fun bind(data: Locations?) {
        data.let {
            this.mMaps = data
            itemView.ivPlace.setImageResource(data?.image!!)
            itemView.tvNamePlace.text = data.name
            itemView.tvDetailPlace.text = data.detail
            itemView.card_view_maps.setOnClickListener {
                mActionListener?.onClicked(this,layoutPosition)
            }
        }
    }

    fun getData() : Locations {
        return mMaps!!
    }

    fun setOnActionListener(listener: onActionListener) {
        mActionListener = listener
    }

    interface onActionListener{
        fun onClicked(view : MapsItemView? , position : Int)
    }

}