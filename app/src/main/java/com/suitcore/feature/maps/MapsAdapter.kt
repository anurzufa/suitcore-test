package com.suitcore.feature.maps

import android.view.ViewGroup
import com.suitcore.R
import com.suitcore.base.ui.adapter.BaseRecyclerAdapter
import com.suitcore.data.model.Locations

class MapsAdapter : BaseRecyclerAdapter<Locations, MapsItemView>() {

    private var mActionListener: MapsItemView.onActionListener? = null

    fun setActionListener(onActionListener: MapsItemView.onActionListener) {
        mActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_maps

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapsItemView {
        val view = MapsItemView(getView(parent))
        mActionListener?.let {
            view.setOnActionListener(it)
        }
        return view
    }
}