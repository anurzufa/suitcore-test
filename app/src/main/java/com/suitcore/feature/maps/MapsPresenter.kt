package com.suitcore.feature.maps

import androidx.lifecycle.LifecycleOwner
import com.suitcore.BaseApplication
import com.suitcore.R
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.model.Locations
import io.reactivex.disposables.CompositeDisposable

class MapsPresenter : BasePresenter<MapsView> {

    private var mapsView: MapsView? = null
    private var mRealmHelperMaps: RealmHelper<Locations>? = RealmHelper()
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getMapsCache() {
        var data: List<Locations>? = mRealmHelperMaps?.getData(Locations())
        if (data == null) data = emptyList()
        mapsView?.onMemberCacheLoaded(data)
    }

    private fun mMapsList(): List<Locations>? {
        val braga = Locations(1, "Braga", "Kec. Sumur Bandung Kota Bandung Jawa Barat", -6.9151158, 107.6089752, R.drawable.place_braga)
        val ciwalk = Locations(2, "Ciampelas Walk", "Jl. Cihampelas No.160, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat", -6.8950665, 107.6040178, R.drawable.place_ciwalk)
        val farm = Locations(3, "Farm House Susu Lembang", "Jl. Raya Lembang No.108, Gudangkahuripan, Lembang, Kabupaten Bandung Barat, Jawa Barat", -6.832660, 107.605789, R.drawable.place_farm_house)
        val gedungsate = Locations(4, "Gedung Sate", "Jl. Diponegoro No.22, Citarum, Kec. Bandung Wetan, Kota Bandung, Jawa Barat ", -6.9024920000000005, 107.61877498466879, R.drawable.place_gedung_sate)
        val trans = Locations(5, "Trans Studio Bandung", "Jalan Jendral Gatot Subroto No.289 A, Cibangkong, Bandung City, West Java ", -6.92521195, 107.63654092338055, R.drawable.place_trans)

        val mapsList: MutableList<Locations> = ArrayList()
        mapsList.add(braga)
        mapsList.add(ciwalk)
        mapsList.add(farm)
        mapsList.add(gedungsate)
        mapsList.add(trans)
        return mapsList
    }

    fun getEvent() {
        mMapsList()?.let {
            saveToCache(it)
        }
        if (mMapsList()?.isNotEmpty()!!) {
            mapsView?.onMemberLoaded(mMapsList())
        } else {
            mapsView?.onMemberEmpty()
        }
        mapsView?.onMemberLoaded(mMapsList())
    }

    private fun saveToCache(data: List<Locations>) {
        mRealmHelperMaps?.saveList(data)
    }


    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: MapsView) {
        mapsView = view
        if (mapsView is LifecycleOwner) {
            (mapsView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mapsView = null
        mCompositeDisposable.let { mCompositeDisposable?.clear() }
    }
}