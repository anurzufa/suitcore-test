package com.suitcore.feature.guest

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import com.google.gson.Gson
import com.suitcore.BaseApplication
import com.suitcore.R
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.model.Guest
import com.suitcore.data.remote.services.APIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GuestPresenter : BasePresenter<GuestView> {

    @Inject
    lateinit var apiService: APIService
    private var mvpView: GuestView? = null
    private var mRealm: RealmHelper<Guest>? = RealmHelper()
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getGuestCache() {
        var data: List<Guest>? = mRealm?.getData(Guest())
        if (data == null) data = emptyList()
        mvpView?.onMemberCacheLoaded(data)
    }

    fun getGuest() {
        mCompositeDisposable?.add(
                apiService.getGuest().map {
                    saveToCache(it)
                    it
                }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ data ->
                            Log.e("TAG", Gson().toJson(data))
                            if (data != null) {
                                if (data.isNotEmpty()) {
                                    mvpView?.onMemberLoaded(data)
                                } else {
                                    mvpView?.onMemberEmpty()
                                }
                                mvpView?.onMemberLoaded(data)
                            } else {
                                mvpView?.onFailed(R.string.txt_error_global)
                            }
                        }, {
                            mvpView?.onFailed(it)
                        })
        )
    }

    private fun saveToCache(data: List<Guest>) {
        mRealm?.saveList(data)
    }


    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: GuestView) {
        mvpView = view
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mvpView = null
        mCompositeDisposable.let { mCompositeDisposable?.clear() }
    }
}