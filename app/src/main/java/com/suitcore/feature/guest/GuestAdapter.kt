package com.suitcore.feature.guest

import android.view.ViewGroup
import com.suitcore.R
import com.suitcore.base.ui.adapter.BaseRecyclerAdapter
import com.suitcore.data.model.Guest

class GuestAdapter : BaseRecyclerAdapter<Guest, GuestItemView>() {

    private var mOnActionListener: GuestItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: GuestItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_guest

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestItemView {
        val view = GuestItemView(getView(parent))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}