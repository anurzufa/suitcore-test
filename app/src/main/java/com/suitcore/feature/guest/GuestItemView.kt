package com.suitcore.feature.guest

import android.view.View
import com.suitcore.R
import com.suitcore.base.ui.adapter.viewholder.BaseItemViewHolder
import com.suitcore.data.model.Guest
import kotlinx.android.synthetic.main.item_guest.view.*

class GuestItemView(itemView: View) : BaseItemViewHolder<Guest>(itemView) {

    private var mActionListener: OnActionListener? = null
    private var guest: Guest? = null

    override fun bind(data: Guest?) {
        data.let {
            this.guest = data
            when (data?.name) {
                "Andi" -> itemView.ivItemPhotoGuest.setBackgroundResource(R.drawable.concert)
                "Budi" -> itemView.ivItemPhotoGuest.setBackgroundResource(R.drawable.livemusic)
                "Charlie" -> itemView.ivItemPhotoGuest.setBackgroundResource(R.drawable.cocktails)
                "Dede" -> itemView.ivItemPhotoGuest.setBackgroundResource(R.drawable.fairground)
                "Joko" -> itemView.ivItemPhotoGuest.setBackgroundResource(R.drawable.concert2)
            }
            itemView.tvNamaGuest.text = data?.name
            itemView.rlItemGuest.setOnClickListener {
                mActionListener?.onClicked(this)
            }
        }
    }

    fun getData(): Guest {
        return guest!!
    }

    fun setOnActionListener(listener: OnActionListener) {
        mActionListener = listener
    }

    interface OnActionListener {
        fun onClicked(view: GuestItemView?)
    }

}