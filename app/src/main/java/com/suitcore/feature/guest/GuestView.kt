package com.suitcore.feature.guest

import com.suitcore.base.presenter.MvpView
import com.suitcore.data.model.Guest

interface GuestView : MvpView {

    fun onMemberCacheLoaded(guests: List<Guest>?)
    fun onMemberLoaded(guests:List<Guest>?)
    fun onMemberEmpty()
    fun onFailed(error : Any?)


}