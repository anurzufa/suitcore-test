package com.suitcore.feature.guest

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.base.ui.recyclerview.BaseRecyclerView
import com.suitcore.data.local.prefs.DataConstant
import com.suitcore.data.local.prefs.SuitPreferences
import com.suitcore.data.model.ErrorCodeHelper
import com.suitcore.data.model.Guest
import com.suitcore.feature.main.MainActivity
import kotlinx.android.synthetic.main.activity_guest.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*


class GuestActivity : BaseActivity(), GuestView, GuestItemView.OnActionListener {

    private var guestPresenter: GuestPresenter? = null
    private var guestAdapter: GuestAdapter? = GuestAdapter()
    private var layoutManager: RecyclerView.LayoutManager? = null


    override val resourceLayout: Int = R.layout.activity_guest

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupProgressView()
        setupToolbar("GuestActivity", toolbarGuest, true)
        setupList()
        setupEmptyView()
        setupErrorView()
        Handler().postDelayed({ setupPresenter() }, 100)

    }

    override fun onDestroy() {
        super.onDestroy()
        clearRecyclerView(rvGuest)
    }

    private fun setupPresenter() {

        guestPresenter = GuestPresenter()
        guestPresenter?.attachView(this)
        guestPresenter?.getGuestCache()

        rvGuest.hideEmpty()
        rvGuest.showShimmer()
        guestPresenter?.getGuest()
    }

    private fun setupList() {
//        val dividerItemDecoration = DividerItemDecoration(rvGuest.context, 2)//decoration give space for recycleview
        rvGuest.apply {
            setUpAsGrid(2)
//            addItemDecoration(dividerItemDecoration)
            setAdapter(guestAdapter)
            setPullToRefreshEnable(true)
            setLoadingMoreEnabled(true)
            setLoadingListener(object : XRecyclerView.LoadingListener {
                override fun onLoadMore() {
                    loadData()
                }

                override fun onRefresh() {
                    loadData()
                }
            })
        }
        guestAdapter?.setOnActionListener(this)
        rvGuest.showShimmer()
    }

    private fun loadData() {
        guestPresenter?.getGuest()
    }

    private fun setData(data: List<Guest>?) {
        data?.let {
            guestAdapter?.add(it)
            rvGuest.stopShimmer()
            rvGuest.showRecycler()
        }
    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_guest.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        rvGuest.setImageEmptyView(R.drawable.empty_state)
        rvGuest.setTitleEmptyView(getString(R.string.txt_empty_member))
        rvGuest.setContentEmptyView(getString(R.string.txt_empty_member_content))
        rvGuest.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })

    }

    private fun setupErrorView() {
        rvGuest.setImageErrorView(R.drawable.empty_state)
        rvGuest.setTitleErrorView(getString(R.string.txt_error_no_internet))
        rvGuest.setContentErrorView(getString(R.string.txt_error_connection))
        rvGuest.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })
    }

    override fun onMemberCacheLoaded(guests: List<Guest>?) {
        guests.let {
            if (guests?.isNotEmpty()!!) {
                setData(guests)
            }
        }
        finishLoad(rvGuest)
    }

    override fun onMemberLoaded(guests: List<Guest>?) {
        guests.let {
            if (guests?.isNotEmpty()!!) {
                setData(guests)
            }
        }
        finishLoad(rvGuest)
    }

    override fun onMemberEmpty() {
        finishLoad(rvGuest)
    }

    override fun onFailed(error: Any?) {
        finishLoad(rvGuest)
        rvGuest.showEmpty()
        error?.let { ErrorCodeHelper.getErrorMessage(this, it)?.let { msg -> showToast(msg) } }
    }

    override fun onClicked(view: GuestItemView?) {
        view?.getData()?.let {
            SuitPreferences.instance()?.saveString(DataConstant.GUEST, it.name)
            goToActivity(MainActivity::class.java, null, clearIntent = true, isFinish = true)
            showToast((it.name))
        }
    }
}
