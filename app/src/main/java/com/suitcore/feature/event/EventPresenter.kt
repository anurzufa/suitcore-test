package com.suitcore.feature.event

import androidx.lifecycle.LifecycleOwner
import com.suitcore.BaseApplication
import com.suitcore.R
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.model.Event
import com.suitcore.data.remote.services.APIService
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EventPresenter : BasePresenter<EventView> {

    private var mvpView: EventView? = null
    private var mRealm: RealmHelper<Event>? = RealmHelper()
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()


    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getEventCache() {
        var data: List<Event>? = mRealm?.getData(Event())
        if (data == null) data = emptyList()
        mvpView?.onMemberCacheLoaded(data)
    }

    private fun mEventList(): List<Event>? {
        val konserRockBand = Event(1,"Konser Rock Band ", "20 November", R.drawable.concert)
        val liverMusicBatu = Event(2,"Liver Music Batu", "15 Desember", R.drawable.livemusic)
        val koktailSosialisasi = Event(3,"Koktail Sosialisasi", "24 Januari", R.drawable.cocktails)
        val gelanggangPekanRaya = Event(4,"Gelanggang Pekan Raya", "27 Februari", R.drawable.fairground)
        val konserKinerjaMusik = Event(5,"Konser Kinerja Musik", "18 Maret", R.drawable.concert2)

        val eventList: MutableList<Event> = ArrayList()
        eventList.add(konserRockBand)
        eventList.add(liverMusicBatu)
        eventList.add(koktailSosialisasi)
        eventList.add(gelanggangPekanRaya)
        eventList.add(konserKinerjaMusik)
        return eventList
    }

    fun getEvent() {
        mEventList()?.let {
            saveToCache(it)
        }
        if (mEventList()?.isNotEmpty()!!) {
            mvpView?.onMemberLoaded(mEventList())
        } else {
            mvpView?.onMemberEmpty()
        }
        mvpView?.onMemberLoaded(mEventList())
    }


    private fun saveToCache(data: List<Event>) {
        mRealm?.saveList(data)
    }

    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: EventView) {
        mvpView = view
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mvpView = null
        mCompositeDisposable.let { mCompositeDisposable?.clear() }
    }
}