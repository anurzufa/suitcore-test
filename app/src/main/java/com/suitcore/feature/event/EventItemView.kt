package com.suitcore.feature.event

import android.view.View
import com.suitcore.base.ui.adapter.viewholder.BaseItemViewHolder
import com.suitcore.data.model.Event
import kotlinx.android.synthetic.main.item_event.view.*

class EventItemView(itemView: View) : BaseItemViewHolder<Event>(itemView) {


    private var mActionListener: OnActionListener? = null
    private var event: Event? = null

    override fun bind(data: Event?) {
        data.let {
            this.event = data
            itemView.tvImageEvent.setImageResource(data?.image!!)
            itemView.tvNameEvent.text = data.nama
            itemView.tvDateEvent.text = data.tanggal
            itemView.card_view_event.setOnClickListener {
                mActionListener?.onClicked(this)
            }
        }

    }

    fun getData(): Event {
        return event!!
    }

    fun setOnActionListener(listener: OnActionListener) {
        mActionListener = listener
    }

    interface OnActionListener {
        fun onClicked(view: EventItemView?)
    }
}