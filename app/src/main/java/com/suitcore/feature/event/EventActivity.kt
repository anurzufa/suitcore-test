package com.suitcore.feature.event

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.base.ui.recyclerview.BaseRecyclerView
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.local.prefs.DataConstant
import com.suitcore.data.local.prefs.SuitPreferences
import com.suitcore.data.model.ErrorCodeHelper
import com.suitcore.data.model.Event
import com.suitcore.feature.main.MainActivity
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

class EventActivity : BaseActivity(), EventView, EventItemView.OnActionListener {

    private var eventPresenter: EventPresenter? = null
    private var eventAdapter: EventAdapter? = EventAdapter()
    private var realmHelper: RealmHelper<Event>? = null

    override val resourceLayout: Int = R.layout.activity_event

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupProgressView()
        setupToolbar("EventActivity",toolbarEvent, true)
        setupList()
        setupEmptyView()
        setupErrorView()
        Handler().postDelayed({ setupPresenter() }, 100)
    }

    override fun onDestroy() {
        super.onDestroy()
        clearRecyclerView(rvEvent)
    }

    private fun setupPresenter() {
        eventPresenter = EventPresenter()
        eventPresenter?.attachView(this)
        eventPresenter?.getEventCache()

        rvEvent.hideEmpty()
        rvEvent.showShimmer()
        eventPresenter?.getEvent()
    }

    private fun setupList() {
        rvEvent.apply {
            setUpAsList()
            setAdapter(eventAdapter)
            setPullToRefreshEnable(true)
            setLoadingMoreEnabled(false)
            setLoadingListener(object : XRecyclerView.LoadingListener {
                override fun onLoadMore() {
                    loadData()
                }
                override fun onRefresh() {
                    loadData()
                }
            })
        }
        eventAdapter?.setOnActionListener(this)
        rvEvent.showShimmer()
    }


    private fun loadData() {
        eventPresenter?.getEvent()
    }

    private fun setData(data: List<Event>?) {
        data?.let {
            eventAdapter?.add(it)
            rvEvent.stopShimmer()
            rvEvent.showRecycler()
        }
    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_guest.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        rvEvent.setImageEmptyView(R.drawable.empty_state)
        rvEvent.setTitleEmptyView(getString(R.string.txt_empty_member))
        rvEvent.setContentEmptyView(getString(R.string.txt_empty_member_content))
        rvEvent.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }
        })

    }

    private fun setupErrorView() {
        rvEvent.setImageErrorView(R.drawable.empty_state)
        rvEvent.setTitleErrorView(getString(R.string.txt_error_no_internet))
        rvEvent.setContentErrorView(getString(R.string.txt_error_connection))
        rvEvent.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })
    }

    override fun onClicked(view: EventItemView?) {
        view?.getData()?.let {
            SuitPreferences.instance()?.saveString(DataConstant.EVENT, it.nama!!)
            goToActivity(MainActivity::class.java, null, clearIntent = true, isFinish = true)
            showToast((it.nama!!))
        }
    }

    override fun onMemberCacheLoaded(event: List<Event>?) {
        event.let {
            if (event?.isNotEmpty()!!) {
                setData(event)
            }
        }
        finishLoad(rvEvent)
    }

    override fun onMemberLoaded(event: List<Event>?) {
        event.let {
            if (event?.isNotEmpty()!!) {
                setData(event)
            }
        }
        finishLoad(rvEvent)
    }

    override fun onMemberEmpty() {
        finishLoad(rvEvent)
    }

    override fun onFailed(error: Any?) {
        finishLoad(rvEvent)
        rvEvent.showEmpty()
        error?.let { ErrorCodeHelper.getErrorMessage(this, it)?.let { msg -> showToast(msg) } }

    }
}
