package com.suitcore.feature.event

import android.view.ViewGroup
import com.suitcore.R
import com.suitcore.base.ui.adapter.BaseRecyclerAdapter
import com.suitcore.data.model.Event

class EventAdapter : BaseRecyclerAdapter<Event, EventItemView>() {

    private var mOnActionListener: EventItemView.OnActionListener? = null
    fun setOnActionListener(onActionListener: EventItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_event

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventItemView {
        val view = EventItemView(getView(parent))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}