package com.suitcore.feature.event

import com.suitcore.base.presenter.MvpView
import com.suitcore.data.model.Event

interface EventView : MvpView {

    fun onMemberCacheLoaded(event: List<Event>?)

    fun onMemberLoaded(event: List<Event>?)

    fun onMemberEmpty()

    fun onFailed(error: Any?)

}