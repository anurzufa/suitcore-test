package com.suitcore.feature.main

import android.os.Bundle
import android.text.TextUtils
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.local.prefs.DataConstant
import com.suitcore.data.local.prefs.SuitPreferences
import com.suitcore.data.model.Event
import com.suitcore.data.model.Guest
import com.suitcore.data.model.User
import com.suitcore.feature.event.EventActivity
import com.suitcore.feature.guest.GuestActivity
import com.suitcore.feature.login.LoginActivity
import com.suitcore.feature.maps.MapsActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), MainView {

    private var mainPresenter: MainPresenter? = null
    override val resourceLayout: Int = R.layout.activity_main

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        btnChooseEvent.setOnClickListener {
            goToActivity(EventActivity::class.java, null, clearIntent = true, isFinish = false)
        }

        btnChooseGuest.setOnClickListener {
            goToActivity(GuestActivity::class.java, null, clearIntent = true, isFinish = false)
        }

        btnMaps.setOnClickListener {
            goToActivity(MapsActivity::class.java, null, clearIntent = true, isFinish = false)
        }

        btnLogout.setOnClickListener {
            SuitPreferences.instance()?.clearSession()
            goToActivity(LoginActivity::class.java, null, clearIntent = true, isFinish = true)
        }
    }

    private fun getData() {
        if (!TextUtils.isEmpty(SuitPreferences.instance()?.getString(DataConstant.EVENT))) {
            btnChooseEvent.text = SuitPreferences.instance()?.getString(DataConstant.EVENT)
        }

        if (!TextUtils.isEmpty(SuitPreferences.instance()?.getString(DataConstant.GUEST))) {
            btnChooseGuest.text = SuitPreferences.instance()?.getString(DataConstant.GUEST)
        }
    }

    private fun setupPresenter() {
        mainPresenter = MainPresenter()
        mainPresenter?.attachView(this)
        mainPresenter?.getUser()
    }

    override fun onSuccess(msg: String) {
        showToast(msg)
    }

    override fun isLogin() {
        goToActivity(LoginActivity::class.java, null, clearIntent = true, isFinish = false)
    }

    override fun onError(msg: String) {
        showToast(msg)
    }

    override fun onUserReceive(user: User?) {
        user?.let {
            tvName.text = user.nama
        }
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

}
