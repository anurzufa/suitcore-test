package com.suitcore.feature.main

import androidx.lifecycle.LifecycleOwner
import com.suitcore.BaseApplication
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.model.User

class MainPresenter : BasePresenter<MainView> {

    private var realmHelperUser: RealmHelper<User>? = RealmHelper()
    private var mvpView: MainView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getUser() {
        val user = realmHelperUser?.getSingleData(User())
        user?.nama
        mvpView?.onUserReceive(user)
    }

    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: MainView) {
        mvpView = view
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mvpView = null
    }
}