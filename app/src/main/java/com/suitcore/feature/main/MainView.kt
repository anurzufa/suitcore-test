package com.suitcore.feature.main

import com.suitcore.data.model.Event
import com.suitcore.data.model.Guest
import com.suitcore.data.model.User

interface MainView {

    fun onSuccess(msg : String)
    fun isLogin()
    fun onError(msg: String)
    fun onUserReceive(user : User?)
}