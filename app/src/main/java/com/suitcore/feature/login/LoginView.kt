package com.suitcore.feature.login

import com.suitcore.data.model.User

interface LoginView {
    fun startNextActivity()
    fun isEmptyField()
}