package com.suitcore.feature.login

import android.os.Bundle
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.model.User
import com.suitcore.feature.main.MainActivity
import kotlinx.android.synthetic.main.activity_first.*


class LoginActivity : BaseActivity(), LoginView {

    private var loginPresenter: LoginPresenter? = null
    override val resourceLayout: Int = R.layout.activity_first

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        btnLogin.setOnClickListener {
            loginPresenter?.startNextActivity(etEnterName.text.toString())
        }
    }

    private fun setupPresenter() {
        loginPresenter = LoginPresenter()
        loginPresenter?.attachView(this)
    }

    override fun startNextActivity() {
        goToActivity(MainActivity::class.java, null, clearIntent = true, isFinish = true)
    }

    override fun isEmptyField() {
        etEnterName.error = "Enter name"
    }

}