package com.suitcore.feature.login

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import com.suitcore.BaseApplication
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.local.prefs.DataConstant
import com.suitcore.data.local.prefs.SuitPreferences
import com.suitcore.data.model.User

class LoginPresenter : BasePresenter<LoginView> {

    private var loginView: LoginView? = null
    private var realmHelperUser: RealmHelper<User>? = RealmHelper()

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun startNextActivity(name: String) {
        if (TextUtils.isEmpty(name)) {
            loginView?.isEmptyField()
        } else {
            val user = User()
            user.id = 1
            user.nama = name
            realmHelperUser?.saveObject(user)
            SuitPreferences.instance()?.saveBoolean(DataConstant.LOGIN, true)
            loginView?.startNextActivity()
        }
    }




    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: LoginView) {
        loginView = view
        if (loginView is LifecycleOwner) {
            (loginView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        loginView = null
    }

}