package com.suitcore.feature.splashscreen

import android.os.Bundle
import com.suitcore.R
import com.suitcore.base.ui.BaseActivity
import com.suitcore.feature.guest.GuestActivity
import com.suitcore.feature.login.LoginActivity
import com.suitcore.feature.main.MainActivity
import com.suitcore.helper.CommonConstant
import kotlinx.android.synthetic.main.activity_splashscreen.*

/**
 * Created by dodydmw19 on 12/19/18.
 */

class SplashScreenActivity : BaseActivity(), SplashScreenView {

    private var splashScreenPresenter: SplashScreenPresenter? = null
    private val actionClicked = ::dialogPositiveAction

    override val resourceLayout: Int = R.layout.activity_splashscreen

    override fun onViewReady(savedInstanceState: Bundle?) {
        changeProgressBarColor(R.color.white, progressBar)
        setupPresenter()
    }

    private fun handleIntent() {
        val data: Bundle? = intent.extras
        if (data?.getString(CommonConstant.APP_CRASH) != null) {
            showConfirmationSingleDialog(getString(R.string.txt_error_crash), actionClicked)
        } else {
            splashScreenPresenter?.checkLogin()
        }
    }

    private fun setupPresenter() {
        splashScreenPresenter = SplashScreenPresenter()
        splashScreenPresenter?.attachView(this)
        splashScreenPresenter?.checkLogin()
        handleIntent()
    }

    override fun navigateToMainView() {
        goToActivity(MainActivity::class.java, null, clearIntent = true, isFinish = true)
    }

    override fun isLoginFalse() {
        goToActivity(LoginActivity::class.java, null, clearIntent = true, isFinish = true)
    }


    private fun dialogPositiveAction() {
        splashScreenPresenter?.checkLogin()
    }

}