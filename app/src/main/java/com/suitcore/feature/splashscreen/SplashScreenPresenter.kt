package com.suitcore.feature.splashscreen

import android.os.Handler
import android.util.Log
import com.suitcore.BaseApplication
import com.suitcore.base.presenter.BasePresenter
import com.suitcore.data.local.RealmHelper
import com.suitcore.data.local.prefs.DataConstant
import com.suitcore.data.local.prefs.SuitPreferences
import com.suitcore.data.model.User

/**
 * Created by dodydmw19 on 12/19/18.
 */

class SplashScreenPresenter : BasePresenter<SplashScreenView> {

    private var mvpView: SplashScreenView? = null
    private val time: Long = 3000
    init {
        BaseApplication.applicationComponent.inject(this)
    }

    private fun isLogin(): Boolean {
        return SuitPreferences.instance()?.getBoolean(DataConstant.LOGIN)!!
    }

    fun checkLogin() {
        if (!isLogin()) {
            Handler().postDelayed({ mvpView?.isLoginFalse() }, time)
        } else {
            Handler().postDelayed({ mvpView?.navigateToMainView() }, time)
        }
    }

    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: SplashScreenView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}