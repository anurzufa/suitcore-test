package com.suitcore.feature.splashscreen

import com.suitcore.base.presenter.MvpView

/**
 * Created by dodydmw19 on 12/19/18.
 */

interface SplashScreenView : MvpView {

    fun navigateToMainView()
    fun isLoginFalse()
}